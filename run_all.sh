#!/bin/bash
m=$1

log="pknn.log"
conf="conf/test.conf"

#pivot selection
hadoop jar pkkn.jar preprocess.ReservoirSelector -conf $conf >>$log                                                     

#data partitioning for 2d data
hadoop jar pknn.jar lof.adapt.PivTreePartitioner ${m} -conf $conf >>$log                                                

#data partitioning for multi-dimensional data
#hadoop jar pknn.jar lof.adapt.AvgPartitioner ${m} -conf $conf >>$log                                                

#To update adaptive bound for previously partitioned data
#hadoop jar pknn.jar lof.adapt.AvgUpdateThresh ${m} -conf $conf >>$log

#knn1
hadoop jar pknn.jar lof.adapt.PkNN_kdist1_Adapt -conf $conf >>$log

#knn 2
hadoop jar pknn.jar lof.adapt.PkNN_kdist2_Adapt -conf $conf >>$log

