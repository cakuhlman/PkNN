/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */
package util;

/**
 * 
 * @author caitlin kuhlman
 *
 */
public class Outlier extends Record {
	
	int origId;
	float dist;
	
	public Outlier(long id, float[] vals, float dist, int origId){
		this.id = id;
		this.vals = vals;
		this.dist = dist;
		this.origId = origId;
	}

	public void setDist(float dist) {
		this.dist = dist;
	}

	public float getDist() {
		return dist;
	}
	
	// get distance to other pivot
	public float dist(Record that) {
		return compL2DistFromFloat(this.vals, that.vals);
	}
	
	public static Outlier readOutlier(String line, int dim) {
		if (line == null || line == "") {
			return null;
		}
		/**
		 * format of the line: id,dim1,dim2,...,dimN,kDist
		 */
		String[] strVector = line.split(",");
		long id = Long.valueOf(strVector[0]);
		float[] vals = new float[dim];
		for (int i = 0; i < dim; i++) {
			vals[i] = Float.valueOf(strVector[i + 1]);
		}
		float dist = Float.valueOf(strVector[dim + 1]);
		int origId = Integer.valueOf(strVector[dim + 2]);

		return new Outlier(id, vals, dist, origId);
	}

	public int getOrigPiv() {
		return origId;
	}
	
	
}
