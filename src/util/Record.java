/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */
/* Record implementation provided by authors of 
 * Lu, Wei, et al. "Efficient processing of k nearest neighbor joins using mapreduce." 
 * Proceedings of the VLDB Endowment 5.10 (2012): 1016-1027.*/

/**
 * Data record containg id and values.
 * Adapted from luwei.
 * @author caitlin kuhlman
 * 
 */
package util;

import java.io.IOException;

public class Record implements Comparable<Record> {
	long id;
	float[] vals;

	public Record(long rid) {
		this.id = rid;
	}

	public Record(long rid, float[] coord) {
		this.id = rid;
		this.vals = coord;
	}

	public Record() {
	}

	public void setRId(long rid) {
		this.id = rid;
	}

	public long getRId() {
		return id;
	}

	public float[] getValue() {
		return vals;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		for (int i = 0; i < vals.length; i++) {
			sb.append("," + vals[i]);
		}

		return sb.toString();
	}

	// sort by id
	public int compareTo(Record r) {
		if (r.id > this.id)
			return -1;
		else if (r.id < this.id)
			return 1;
		else
			return 0;
	}

	public static float compL1Dist(Record r1, Record r2) {
		{
			float dist = 0f;
			float[] f1 = r1.getValue();
			float[] f2 = r2.getValue();

			for (int i = 0; i < f1.length; i++) {
				dist += Math.abs(f1[i] - f2[i]);
			}

			return dist;
		}
	}

	public static float compL1DistFromFloat(float[] o1, float[] o2) {
		float dist = 0f;
		for (int i = 0; i < o1.length; i++) {
			dist += Math.abs(o1[i] - o2[i]);
		}
		return dist;
	}

	public static float compL2Dist(Record r1, Record r2) throws IOException {
		float dist = 0f;
		float[] f1 = r1.getValue();
		float[] f2 = r2.getValue();

		for (int i = 0; i < f1.length; i++) {
			dist += (f1[i] - f2[i]) * (f1[i] - f2[i]);
		}

		return (float) Math.sqrt(dist);
	}

	public static float compL2DistFromFloat(float[] o1, float[] o2) {
		float dist = 0f;
		for (int i = 0; i < o1.length; i++) {
			dist += (o1[i] - o2[i]) * (o1[i] - o2[i]);
		}

		return (float) Math.sqrt(dist);
	}

	public void setVals(float[] vals) {
		this.vals = vals;

	}
}
