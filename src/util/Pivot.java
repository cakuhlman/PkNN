/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */
/* Record implementation provided by authors of 
 * Lu, Wei, et al. "Efficient processing of k nearest neighbor joins using mapreduce." 
 * Proceedings of the VLDB Endowment 5.10 (2012): 1016-1027.*/

package util;

/**
 * 
 * @author caitlin kuhlman
 *
 */
public class Pivot extends Record {

	int id;
	// dist used for boundingDist in round 1
	// and for maxKDist in round 2
	float dist;
	// after first round this is max(d(piv, pt) + 2(kdit(pt)))
	float maxSuppDist;
	// number of points mapped to this pivot in datasplitting
	int size;
	//adaptive threshold bound computed in datasplitting
	float thresh;
	//distance to farthest point in cell
	float maxDist;

	public Pivot(int id, float[] vals, float dist) {
		this.id = id;
		this.vals = vals;
		this.dist = dist;
	}

	public Pivot(int id, float[] vals) {
		this.id = id;
		this.vals = vals;
	}

	public Pivot(int id, float[] vals, float dist, int size) {
		this.id = id;
		this.vals = vals;
		this.dist = dist;
		this.size = size;
	}

	public Pivot(int id, float[] vals, float dist, float thresh) {
		this.id = id;
		this.vals = vals;
		this.dist = dist;
		this.thresh = thresh;
	}

	// get distance to other pivot
	public float dist(Record that) {
		return compL2DistFromFloat(this.vals, that.vals);
	}

	public float getMaxSupDist() {
		return maxSuppDist;
	}

	public void setSupDist(float maxSupDist) {
		this.maxSuppDist = maxSupDist;
	}

	public void setDist(float dist) {
		this.dist = dist;
	}

	public float getDist() {
		return dist;
	}

	public float getSize() {
		return size;
	}
	
	public float getThresh() {
		return thresh;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * parse a pivot from a given line
	 */
	public static Pivot readPivot(String line, int dim) {
		if (line == null || line == "") {
			return null;
		}
		/**
		 * format of the line: id,dim1,dim2,...,dimN,boundingDist
		 */
		String[] strVector = line.split(",");
		int id = Integer.valueOf(strVector[0]);
		float[] vals = new float[dim];
		for (int i = 0; i < dim; i++) {
			vals[i] = Float.valueOf(strVector[i + 1]);
		}

		return new Pivot(id, vals);
	}

	public static Pivot readPivotAdapt(String line, int dim) {
		if (line == null || line == "") {
			return null;
		}
		/**
		 * format of line: id,vals,ibound,size,maxDist,avgDist,thresh
		 */
		String[] strVector = line.split(",");
		int id = Integer.valueOf(strVector[0]);
		float[] vals = new float[dim];
		for (int i = 0; i < dim; i++) {
			vals[i] = Float.valueOf(strVector[i + 1]);
		}
		float ibound = Float.valueOf(strVector[dim + 1]);
//		int size = Integer.valueOf(strVector[dim + 2]);
//		float dist = Float.valueOf(strVector[dim + 3]);
		float thresh = Float.valueOf(strVector[dim + 5]);

		return new Pivot(id, vals, ibound, thresh);
	}
	
	public static Pivot readPivotAndDist(String line, int dim) {
		if (line == null || line == "") {
			return null;
		}
		/**
		 * format of the line: id,dim1,dim2,...,dimN,boundingDist
		 */
		String[] strVector = line.split(",");
		int id = Integer.valueOf(strVector[0]);
		float[] vals = new float[dim];
		for (int i = 0; i < dim; i++) {
			vals[i] = Float.valueOf(strVector[i + 1]);
		}
		float dist = Float.valueOf(strVector[dim + 1]);

		return new Pivot(id, vals, dist);
	}

	public int getId() {
		return id;
	}

	public void setVals(float[] sums) {
		this.vals = sums;
	}

	public void incSize() {
		size++;
	}

	public float getMaxDist() {
		return maxDist;
	}

	public void setMaxDist(float maxDist) {
		this.maxDist = maxDist;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(id);
		for (int i = 0; i < vals.length; i++) {
			sb.append("," + vals[i]);
		}

		return sb.toString();
	}
}
