/* implementation provided by authors of 
 * Lu, Wei, et al. "Efficient processing of k nearest neighbor joins using mapreduce." 
 * Proceedings of the VLDB Endowment 5.10 (2012): 1016-1027.*/

/**
 * Configuration file adapted from luwei.
 * @author caitlin kuhlman
 * 
 */
package util;

public class Config {
	/** metric space */
	public static final String strMetricSpace = "metricspace.dataspace";  
	/** metric */
	public static final String strMetric = "metricspace.metric";
	/** number of K */
	public static final String strK = "K";
	/** number of dimensions */
	public static final String strDimExpression = "vector.dim";
	/**============================= pivot selection ================ */
	/** path for pivots*/
	public static final String strPivotInput = "pivot.input";   
	public static final String strPivotDrop = "pivot.dropped";
	public static final String strPivotCount = "support.count";
	public static final String strPivotKDistInput = "pivot.kdist.input";
	public static final String strSelStrategy = "pivot.selection.strategy";  
	public static final String strSampleSize = "pivot.sample.size";
	public static final String pivotSamplePercent = "sample.percent";
	
	/** dataset original path*/
	public static final String dataset = "dataset.input.dir";   
	
	
	/** file extension names of indexes, maintained for DistributedCache */
	public static final String strPivotExpression = ".pivot";
	public static final String strKdistExpression = ".kdist";
	public static final String strGroupExpression = ".group";
	public static final String outlierExpression = ".outlier";
	public static final String strDropExpression = ".drop";
	
	/** seperator for items of every record in the index */
	public static final String sepStrForIndex = ",";
	public static final String sepStrForRecord = ",";
	public static final String sepStrForKeyValue = "\t";
	public static final String sepStrForIDDist = "|";
	public static final String sepSplitForIDDist = "\\|";
	
	public static final String strNumOfReducers = "reducer.count";
	public static final String strNumOfPivots = "pivot.count";
	
	/** Output folders **/
	public static final String strLofInput = "datasplit.output";
	public static final String strKdistanceOutput = "kdistance.output";
	public static final String strKdistance1Output = "kdistance1.output";
	public static final String strEarly = "early.output";
	public static final String strCount = "count";
	
	/** pivot selection **/
	public static final String pivotOutput = "pivot.selection";
	public static final String pivotOutput2 = "pivot.refine";
	public static final String pivotOutput3 = "pivot.boundingdist";
	public static final String outliers = "globalOutliers";
	public static final String suppCounts = "suppCounts";
	public static final String strSizes= "partition.size";
	
}
