/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */

/**
 * @author caitlin kuhlman
 */
package util;

public class PivotInfo {
	public int id;
	public float dist;

	public PivotInfo(int id, float dist) {
		this.id = id;
		this.dist = dist;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getDist() {
		return dist;
	}

	public void setDist(float dist) {
		this.dist = dist;
	}
}