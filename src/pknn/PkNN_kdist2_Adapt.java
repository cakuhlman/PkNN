/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */
package pknn;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;

import metricspace.IMetric;
import metricspace.IMetricSpace;
import metricspace.MetricFactory.L2Metric;
import metricspace.MetricObject;
import metricspace.MetricSpaceFactory.VectorSpace;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import util.Config;
import util.EarlyOutput;
import util.Outlier;
import util.Pivot;
import util.PriorityQueue;
import util.Record;

/**
 * Process partition and calculate k-distance for core area (1) Use DataSplit
 * Mapper to assign points to pivots (2) in reduce function, compute k-distance
 * for each object in core area.
 * 
 * @author Yizhou modified Luwei
 * @author Caitlin Kuhlman
 * 
 */

// FINAL OUTPUT FORMAT: point_id <tab> myPartiton_id, thisParition_id,
// k-dist, whoseSupport, neighbors
public class PkNN_kdist2_Adapt {

	private static int dim;

	public static class PkKdist2Mapper extends
			Mapper<LongWritable, Text, LongWritable, Text> {

		MultipleOutputs<LongWritable, Text> mos;
		/**
		 * VectorSpace and L1 are set as the default metric space and metric.
		 */
		private IMetricSpace metricSpace = null;
		private IMetric metric = null;
		private int dim;

		private static HashMap<Integer, Pivot> pivots;
		private static HashMap<Long, Integer[]> pivSupports;
		private static HashMap<Long, Long[]> outSupports;
		private static HashMap<Long, Outlier> outliers;
		int K;
		/** intermediate key */
		private LongWritable pivotKey = new LongWritable();
		private LongWritable pointKey = new LongWritable();
		private Text pointValue = new Text();
		private static int maxPivot;

		/** number of object pairs to be computed */
		static enum Counters {
			sum
		}

		/**
		 * get MetricSpace and metric from configuration
		 * 
		 * @param conf
		 * @throws IOException
		 */
		private void readMetricAndMetricSpace(Configuration conf)
				throws IOException {

			metricSpace = new VectorSpace();
			metric = new L2Metric();
			metricSpace.setMetric(metric);
		}

		/**
		 * read pivots, maxkdist for each parititon, and global outlier info
		 * from input
		 * 
		 * @param conf
		 */
		private void readPivotFile(Configuration conf, Context context)
				throws IOException {

			URI[] pivotFiles = new URI[0];

			pivotFiles = context.getCacheFiles();
			if (pivotFiles == null || pivotFiles.length < 1)
				throw new IOException("No pivots are provided!");
			String pivotFile = null;
			String kdistFile = null;
			String outlierFile = null;
			for (URI path : pivotFiles) {
				String filename = path.toString();
				if (filename.endsWith(Config.strPivotExpression))
					pivotFile = filename;
				else if (filename.endsWith(Config.strKdistExpression))
					kdistFile = filename;
				else if (filename.endsWith(Config.outlierExpression))
					outlierFile = filename;
			}
			readPivots(pivotFile, kdistFile, outlierFile, metricSpace, dim,
					conf);
		}

		/**
		 * read pivots from a single file
		 * 
		 * @param pivotFile
		 * @param maxKdistFile
		 * @return
		 * @throws IOException
		 */
		public static void readPivots(String pivotFile, String maxKdistFile,
				String outlierFile, IMetricSpace metricSpace, int dim,
				org.apache.hadoop.conf.Configuration conf) throws IOException {

			pivots = new HashMap<Integer, Pivot>();
			outliers = new HashMap<Long, Outlier>();
			FileSystem fs = FileSystem.get(conf);
			BufferedReader fis = null;
			FSDataInputStream p_in = fs.open(new Path(pivotFile));
			FSDataInputStream k_in = fs.open(new Path(maxKdistFile));
			try {
				fis = new BufferedReader(new InputStreamReader(p_in));
				String line;
				while ((line = fis.readLine()) != null) {
					// ignore bounding dist from first round
					Pivot p = Pivot.readPivot(line, dim);
					pivots.put(p.getId(), p);
				}
				// look up kdist for each
				fis = new BufferedReader(new InputStreamReader(k_in));
				while ((line = fis.readLine()) != null) {
					// format of the line: id maxKdist
					String[] strVector = line.split("\t");
					int id = Integer.valueOf(strVector[0]);
					String[] dists = strVector[1].split(",");
					pivots.get(id).setDist(Float.parseFloat(dists[0]));
					pivots.get(id).setSupDist(Float.parseFloat(dists[1]));

				}

				// get outliers
				p_in = fs.open(new Path(outlierFile));
				fis = new BufferedReader(new InputStreamReader(p_in));

				// each global outlier id = the original point id + the number
				// of pivots.
				// organize pivots by
				while ((line = fis.readLine()) != null) {
					// outlier pivots have kdist in-line
					Outlier o = Outlier.readOutlier(line, dim);
					outliers.put(o.getRId(), o);
				}
			} finally {
				if (p_in != null) {
					fis.close();
					p_in.close();
				}
				if (k_in != null) {
					fis.close();
					k_in.close();
				}
			}
		}

		/**
		 * Called once at the beginning of the task. In setup, we (1) init
		 * metric and metric space (2) read pivots (3) init matrix
		 */
		protected void setup(Context context) throws IOException,
				InterruptedException {

			mos = new MultipleOutputs<LongWritable, Text>(context);

			Configuration conf = context.getConfiguration();
			/** get K */
			K = Integer.valueOf(conf.get(Config.strK, "3"));
			/** get dim */
			dim = conf.getInt(Config.strDimExpression, 2);
			/** read metric */
			readMetricAndMetricSpace(conf);
			/** read pivot set */
			if (pivots == null) {
				readPivotFile(conf, context);
			}
			maxPivot = conf.getInt(Config.strNumOfPivots, 0) + 1;

			// preprocess pivots to limit support area search - only once per
			// jvm
			if (pivSupports == null) {
				pivSupports = new HashMap<Long, Integer[]>();
				for (int i : pivots.keySet()) {
					Pivot p = pivots.get(i);

					ArrayList<Integer> sups = new ArrayList<Integer>();

					for (int j : pivots.keySet()) {
						if (i != j) {
							Pivot q = pivots.get(j);
							// check support bound
							if ((p.dist(q) / 2.0) < q.getMaxSupDist()) {
								sups.add(j);
							}
						}
					}
					pivSupports.put((long) i,
							sups.toArray(new Integer[sups.size()]));
				}
				// preprocess outliers to limit support area search - only once
				// per
				// jvm
				if (outSupports == null) {

					outSupports = new HashMap<Long, Long[]>();

					for (int i : pivots.keySet()) {

						Pivot p = pivots.get(i);

						ArrayList<Long> sups = new ArrayList<Long>();

						for (long j : outliers.keySet()) {
							Outlier o = outliers.get(j);
							if (o.getOrigPiv() != p.getId()) {

								Pivot orig = pivots.get(o.getOrigPiv());
								float suppDist = o.dist(orig) + o.getDist();
								float hpDist = p.dist(orig) / 2;
								// check support bound using original pivot
								if (hpDist < suppDist) {
									sups.add(j);
								}
							}
						}
						Long[] s = sups.toArray(new Long[sups.size()]);
						outSupports.put((long) i, s);
					}
				}
			}
		}

		/**
		 * We generate
		 * 
		 * (1) key: the pivot id (int)
		 * 
		 * (2) value: point_id, point_vals, dist to pivot, k-distance, (KNN's
		 * nid and dist), completion tag
		 * 
		 * @key: pivot id
		 * @value: format: point id, p_vals, distToPivot, k-distance,
		 *         whoseSupport, (KNN's nid and dist), completion tag
		 */
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {

			String[] vals = value.toString().split("\t");
			long pivotId;
			Record thisPivot = null;
			try {
				int id = Integer.valueOf(vals[0]);

				if (pivots.containsKey(id)) {
					thisPivot = pivots.get(id);
				} else {
					thisPivot = outliers.get(id);
				}
				pivotId = id;
			} catch (Exception e) {
				// if id is long then this is outlier
				pivotId = Long.valueOf(vals[0]);
				thisPivot = outliers.get(pivotId);
			}

			String line = vals[1];

			// Parse line
			String[] split = line.split(Config.sepStrForRecord);
			long pid = Long.parseLong(split[0]);
			pointKey.set(pid);
			float[] p_vals = new float[dim];
			String val_string = "";
			for (int i = 0; i < dim; i++) {
				p_vals[i] = Float.parseFloat(split[i + 1]);
				val_string += split[i + 1];
				val_string += ",";
			}

			// get dists
			float distToPivot = Float.parseFloat(split[dim + 1]);
			float kdist = Float.parseFloat(split[dim + 2]);
			String neighbor_info = "";
			for (int i = 0; i < K; i++) {
				neighbor_info += split[i + dim + 3];
				neighbor_info += ",";
			}

			/** check if this point should go to supporting area of other pivots */
			Vector<Long> supportList = null;

			// global outliers.
			if (split[split.length - 1].equals("o")) {

				int origPivot = Integer.valueOf(split[split.length - 2]);

				// check if support of other partitions using original pivot
				supportList = getSuppList(p_vals, pivots.get(origPivot),
						pivSupports.get((long) origPivot));

				// check if support of global outliers
				getOutSuppList(origPivot, p_vals, supportList);

			}

			// regular points
			else {

				// check if support of other partitions
				supportList = getSuppList(p_vals, thisPivot,
						pivSupports.get(pivotId));

				// check if support of global outliers
				getOutSuppList(pivotId, p_vals, supportList);

			}

			/*********************
			 * INTERMEDIATE FORMAT: point id, p_vals, distToPivot, k-distance,
			 * type, whoseSupport,
			 * ****************************************(KNN's nid and dist),
			 * completion tag
			 */

			// write out duplicates of point for each partition it supports
			StringBuffer newline;
			// string used in later rounds for indexing
			String whoseSupport = "";

			if (supportList.size() > 0) {
				newline = new StringBuffer();
				newline.append(pid);
				newline.append(",");
				newline.append(val_string);
				newline.append(distToPivot);
				newline.append(",");
				newline.append(kdist);
				newline.append(",,");// for supporting points, no whoseSupport,
				newline.append(neighbor_info);
				newline.append("S");

				StringBuffer sb = new StringBuffer();
				for (long i : supportList) {
					sb.append(i);
					sb.append("|");

					// write to partition
					pivotKey.set(i);
					pointValue.set(newline.toString());
					context.write(pivotKey, pointValue);
					// mos.write("support", pivotKey, pointValue);
				}

				whoseSupport = sb.substring(0, sb.length() - 1);
			}

			/****************** OUTPUT TO REDUCERS *****************************/
			// write out point to it's core partition
			newline = new StringBuffer();
			newline.append(pid);
			newline.append(",");
			newline.append(val_string);
			newline.append(distToPivot);
			newline.append(",");
			newline.append(kdist);
			newline.append(",");
			newline.append(whoseSupport);
			newline.append(",");
			newline.append(neighbor_info);
			newline.append("C");
			if (split[split.length - 1].equals("o")) {
				pivotKey.set(pid + maxPivot);
			} else {
				pivotKey.set(pivotId);
			}
			pointValue.set(newline.substring(0, newline.length()));

			// if incomplete from first round, write to core partition
			if (split[split.length - 1].equals("i")
					|| split[split.length - 1].equals("o")) {
				context.write(pivotKey, pointValue);
			} else if (split[split.length - 1].equals("c")) {

				/******************* EARLY OUTPUT ***********************************/

				// FINAL OUTPUT FORMAT: point_id <tab> myPartiton_id,
				// thisParition_id, whoseSupport, neighbors
				String earlyStr = neighbor_info;
				pointValue.set(earlyStr.substring(0, earlyStr.length() - 1));
				mos.write("earlyOutput", pointKey, pointValue);
				context.getCounter(EarlyOutput.EARLY_RECORDS).increment(1);
			}
		}

		/**
		 * @param pivotId
		 * @param p_vals
		 * @param supportList
		 */
		public void getOutSuppList(long pivotId, float[] p_vals,
				Vector<Long> supportList) {
			for (long i : outSupports.get(pivotId)) {
				Outlier o = outliers.get(i);

				float distToGO = metric.distFromFloat(o.getValue(), p_vals);

				if (Float.compare(distToGO, o.getDist()) <= 0) {
					supportList.add(o.getRId());
				}
			}
		}

		/**
		 * 
		 * @param p_vals
		 *            point values
		 * @param thisPivot
		 * @param pivSupps
		 *            list of possible partitions that this point can support
		 * @return
		 * @throws IOException
		 */
		public Vector<Long> getSuppList(float[] p_vals, Record thisPivot,
				Integer[] pivSupps) throws IOException {

			/** check if this point should go to supporting area of other pivots */
			Vector<Long> supportList = new Vector<Long>();

			for (int i : pivSupps) {
				Pivot thatPivot = pivots.get(i);

				float pivDist = metric.distFromFloat(p_vals,
						thatPivot.getValue());

				if (pivDist < thatPivot.getMaxSupDist()) {
					float hdist = getDistToHP(p_vals, thisPivot.getValue(),
							thatPivot.getValue());

					float middist = metric.dist(thatPivot, thisPivot);

					if (assignSupport(hdist, thatPivot.getDist(),
							thatPivot.getMaxSupDist() - middist))
						supportList.add((long) thatPivot.getId());
				}
			}
			return supportList;
		}

		/**
		 * Compute the distance from a point to the hyperplane between two
		 * pivots.
		 * 
		 * @param x
		 *            the query point.
		 * @param p_x
		 *            the pivot for x's partition.
		 * @param p_i
		 *            neighboring pivot.
		 * @return distance.
		 */
		public static float getDistToHP(float[] x, float[] p_x, float[] p_i)
				throws IllegalArgumentException {

			if (Arrays.equals(p_x, p_i)) {
				throw new IllegalArgumentException();
			}
			// find point on hyperplane
			float[] midpoint = new float[p_x.length];
			for (int i = 0; i < p_x.length; i++) {
				midpoint[i] = (p_x[i] + p_i[i]) / 2;
			}

			// find normal vector (a,b,c)
			float[] norm = new float[p_x.length];
			for (int i = 0; i < p_x.length; i++) {
				norm[i] = (p_i[i] - p_x[i]);
			}

			// find d term (plane in 3d = ax+by+cz+d =0)
			float d = 0f;
			for (int i = 0; i < p_x.length; i++) {
				d += norm[i] * midpoint[i];
			}
			d *= -1;

			// find distance
			// query point x = (x,y,z)
			// D = |ax + by + cz + d| / sqrt(a^2+b^2+c^2)
			float denom = 0f;

			for (int i = 0; i < p_x.length; i++) {
				denom += Math.pow(norm[i], 2);
			}
			denom = (float) Math.sqrt(denom);

			float dist = 0f;
			for (int i = 0; i < p_x.length; i++) {
				dist += norm[i] * x[i];
			}
			dist = Math.abs(dist + d);

			return Math.abs(dist / denom);
		}

		static boolean assignSupport(float hdist, float coredist, float suppdist) {

			// float dist = Math.min(coredist, suppdist);
			// if (Float.compare(hdist, dist) < 0) {
			if (Float.compare(hdist, coredist) < 0) {
				return true;
			} else
				return false;
		}

		@Override
		public void cleanup(Context context) throws IOException,
				InterruptedException {
			mos.close();
		}
	}

	public static class PkKdist2Reducer extends
			Reducer<LongWritable, Text, LongWritable, Text> {

		MultipleOutputs<LongWritable, Text> mos;
		private IMetricSpace metricSpace = null;
		private IMetric metric = null;
		int K;
		static LongWritable outputKey = new LongWritable();
		static Text outputValue = new Text();
		float boundingDistance;
		static PivotBasedKNNSearch search;
		static PriorityQueue pq;
		static int maxPivot;

		/**
		 * get MetricSpace and metric from configuration
		 * 
		 * @param conf
		 * @throws IOException
		 */
		private void readMetricAndMetricSpace(Configuration conf)
				throws IOException {

			metricSpace = new VectorSpace();
			metric = new L2Metric();
			metricSpace.setMetric(metric);
		}

		protected void setup(Context context) throws IOException,
				InterruptedException {

			Configuration conf = context.getConfiguration();
			mos = new MultipleOutputs<LongWritable, Text>(context);
			dim = conf.getInt(Config.strDimExpression, 2);
			readMetricAndMetricSpace(conf);
			K = Integer.valueOf(conf.get(Config.strK, "3"));
			pq = new PriorityQueue(PriorityQueue.SORT_ORDER_DESCENDING);
			search = new PivotBasedKNNSearch(metric, K);
			maxPivot = conf.getInt(Config.strNumOfPivots, 0) + 1;
		}

		/**
		 * 
		 * @param key
		 * @param split
		 *            format: point id, p_vals, distToPivot, k-distance,
		 *            whoseSupport (KNN's nid and dist), completion tag
		 * @return
		 */
		private MetricObject parseObject(long pivotId, String[] split) {

			long pid = Long.parseLong(split[0]);
			float[] p_vals = new float[dim];
			for (int i = 0; i < dim; i++) {

				p_vals[i] = Float.parseFloat(split[i + 1]);
			}
			float distToPivot = Float.parseFloat(split[dim + 1]);
			float kdist = Float.parseFloat(split[dim + 2]);
			String whoseSupport = split[dim + 3];
			String[] knnInfo = new String[K];
			for (int i = 0; i < K; i++) {
				knnInfo[i] = split[dim + 4 + i];
			}
			char tag = split[split.length - 1].charAt(0);
			// System.out.println(tag);
			Record record = new Record(pid, p_vals);
			return new MetricObject(pid, pivotId, distToPivot, kdist,
					whoseSupport, knnInfo, record, tag);

		}

		private MetricObject parseOutlierSupport(String[] split)
				throws IOException {

			long pid = Long.parseLong(split[0]);
			float[] p_vals = new float[dim];
			for (int i = 0; i < dim; i++) {

				p_vals[i] = Float.parseFloat(split[i + 1]);
			}
			Record record = new Record(pid, p_vals);

			return new MetricObject(record);

		}

		private MetricObject parseOutlier(String[] split, float theta) {

			long pid = Long.parseLong(split[0]);

			float[] p_vals = new float[dim];
			for (int i = 0; i < dim; i++) {

				p_vals[i] = Float.parseFloat(split[i + 1]);
			}

			float kdist = Float.parseFloat(split[dim + 2]);

			String[] knnInfo = new String[K];
			for (int i = 0; i < K; i++) {
				knnInfo[i] = split[dim + 4 + i];
			}

			return new MetricObject(pid, new Record(pid, p_vals), kdist,
					knnInfo);
		}

		/**
		 * find knn for each string in the key.pid format of each value in
		 * values
		 * 
		 * @param key
		 *            : pivot id
		 * @param values
		 *            format: partition id, distToPivot, p_vals, k-distance,
		 *            (KNN's nid and dist), completion tag
		 * 
		 *            output format:nid || value: partition id, pid, k-distance,
		 *            whoseSupport, (KNN's nid and dist)
		 */
		public void reduce(LongWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {

			long pivotId = key.get();

			if (pivotId >= maxPivot) // handle global outliers
			{
				pq.clear();

				float theta = Float.MAX_VALUE;
				MetricObject mo;
				MetricObject o = null;
				int count = 0;
				ArrayList<MetricObject> support = new ArrayList<MetricObject>();

				for (Text line : values) {

					String[] split = line.toString().split(",");

					if (split[split.length - 1].charAt(0) == 'C') {

						// this is the global outlier point
						o = parseOutlier(split, theta);

						// include neighbors found in previous round
						String[] s;
						for (String n : o.knnInfo) {
							s = n.split("\\|");

							theta = search.updateQueue(Float.parseFloat(s[1]),
									pq, theta, Long.parseLong(s[0]));
						}
					} else {
						support.add(mo = parseOutlierSupport(split));

					}
				}
				for (MetricObject m : support) {
					theta = search.updateQueue(metric.dist(m.record, o.record),
							pq, theta, m.getId());
					count++;
				}

				// guaranteed to have found k neighbors, write out
				String line = "";

				if (pq.size() > 0) {
					line += pq.getValue() + Config.sepStrForIDDist
							+ pq.getPriority();
					pq.pop();
				}
				while (pq.size() > 0) {
					line += Config.sepStrForRecord + pq.getValue()
							+ Config.sepStrForIDDist + pq.getPriority();
					pq.pop();
				}
				outputValue.set(line);
				outputKey.set(o.getId());
				context.write(outputKey, outputValue);

				// support points, core points
				outputValue.set("out " + count);
				mos.write(key, outputValue, "numSupport");

			}

			else // handle regular partitions
			{
				ArrayList<MetricObject> sortedData = new ArrayList<MetricObject>();
				MetricObject mo;
				for (Text line : values) {

					String[] split = line.toString().split(",");

					mo = parseObject(pivotId, split);
					sortedData.add(mo);
				}

				Collections.sort(sortedData, new Comparator<MetricObject>() {
					public int compare(MetricObject map1, MetricObject map2) {
						return Float
								.compare(map2.distToPivot, map1.distToPivot);
					}
				});

				int supportPoints = 0;
				int corePoints = 0;

				MetricObject thisPoint = null;
				for (int i = 0; i < sortedData.size(); i++) {
					thisPoint = sortedData.get(i);

					// find knn for all core points
					if (thisPoint.type == 'C') {
						String line = search.findKNNSecondRoundPivot(thisPoint,
								i, sortedData);
						corePoints++;
						outputValue.set(line);
						outputKey.set(thisPoint.getId());
						context.write(outputKey, outputValue);
					} else {
						supportPoints++;
					}
				}

				outputKey.set(key.get());
				// support points, core points
				outputValue.set(corePoints + "," + supportPoints);
				mos.write(outputKey, outputValue, "numSupport");
			}
		}

		@Override
		public void cleanup(Context context) throws IOException,
				InterruptedException {
			mos.close();
		}
	}

	public void run(String[] args) throws Exception {

		/********************** SETUP *********************************/
		Configuration conf = new Configuration();
		conf.addResource(new Path(
				"/usr/local/Cellar/hadoop/etc/hadoop/core-site.xml"));
		conf.addResource(new Path(
				"/usr/local/Cellar/hadoop/etc/hadoop/hdfs-site.xml"));

		new GenericOptionsParser(conf, args).getRemainingArgs();
		Job job = Job.getInstance(conf, "Random Pivot Selection");
		FileSystem fs = FileSystem.get(conf);
		Path input = new Path(conf.get(Config.dataset));
		Path output = new Path(conf.get(Config.pivotOutput));

		dim = conf.getInt(Config.strDimExpression, 2);
		int k = conf.getInt(Config.strK, 3);
		long begin;
		long end;
		long seconds;
		Counters counters;
		CounterGroup group;

		/******************* RUN KDIST2 *******************************/

		job = Job.getInstance(conf, "Calculate k-distance round 2");

		job.setJarByClass(PkNN_kdist2_Adapt.class);
		job.setMapperClass(PkKdist2Mapper.class);
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setReducerClass(PkKdist2Reducer.class);
		job.setNumReduceTasks(conf.getInt(Config.strNumOfReducers, 1));

//		String strFSName = conf.get("fs.default.name");
		String strFSName = "/home/caitlin/Documents/PkNN_public/";
		
		input = new Path(strFSName + conf.get(Config.strKdistance1Output));
		output = new Path(strFSName + conf.get(Config.strKdistanceOutput));
		URI pivs = new URI(strFSName + conf.get(Config.strPivotInput));
		URI kdists = new URI(strFSName + conf.get(Config.strPivotKDistInput));
		URI outliers = new URI(strFSName + conf.get(Config.outliers));

		FileInputFormat.addInputPath(job, input);
		job.addCacheFile(pivs);
		job.addCacheFile(kdists);
		job.addCacheFile(outliers);

		fs = FileSystem.get(conf);
		fs.delete(output, true);
		FileOutputFormat.setOutputPath(job, output);

		MultipleOutputs.addNamedOutput(job, "earlyOutput",
				TextOutputFormat.class, LongWritable.class, Text.class);
		MultipleOutputs.addNamedOutput(job, "globaloutliers",
				TextOutputFormat.class, LongWritable.class, Text.class);

		System.err.println("Running Kdist2");

		// /** print job parameter */
		System.err.println("input path: " + input);
		System.err.println("output path: " + output);
		System.err.println("pivot file: " + pivs);
		System.err.println("kdist file: " + kdists);
		System.err.println("outlier file: " + outliers);
		System.err.println("dataspace: " + conf.get(Config.strMetricSpace));
		System.err.println("metric: " + conf.get(Config.strMetric));
		System.err.println("value of K: " + k);
		System.err.println("# of groups: "
				+ conf.get(Config.strNumOfReducers));
		System.err.println("# of dim: "
				+ conf.getInt(Config.strDimExpression, 2));

		begin = System.currentTimeMillis();
		job.waitForCompletion(true);

		end = System.currentTimeMillis();
		seconds = (end - begin) / 1000;

		counters = job.getCounters();
		group = counters.getGroup("org.apache.hadoop.mapreduce.TaskCounter");

		float dataSize = group.findCounter("MAP_INPUT_RECORDS").getValue();

		float supp = group.findCounter("REDUCE_INPUT_RECORDS").getValue(); // support+core
																			// for
																			// second
																			// knn
																			// search
		float early = counters.findCounter(EarlyOutput.EARLY_RECORDS)
				.getValue(); // early output
		float dup_rate = (supp + early) / dataSize;

		// System.out.println("Method, datasize, num_pivots, time, dup_rate, early_output, supp_pivots");
		System.out.print("kdist_2,");
		System.out.print(dataSize + ","); // size of dataset
		System.out.print(group.findCounter("REDUCE_INPUT_GROUPS").getValue()
				+ ","); // number of pivots
		System.out.print(seconds + ",");
		System.out.print(dup_rate + ",");
		System.out.println(early);

		/*********************** GET OUTPUT *****************************/

		fs = FileSystem.get(conf);
		FileStatus[] stats = fs.listStatus(output);
		InputStream is = null;

		OutputStream suppCounts = fs.create(new Path(strFSName
				+ conf.get(Config.suppCounts)));

		try {
			for (int i = 0; i < stats.length; ++i) {
				String fileName = output.toString() + "/"
						+ stats[i].getPath().getName();

				if (fileName.contains("numSupport")) {
					is = new BufferedInputStream(fs.open(new Path(fileName)));
					IOUtils.copyBytes(is, suppCounts, conf, false);
					// fs.delete(stats[i].getPath(), true);
					is.close();

				}
			}
		} finally {
			suppCounts.close();
		}

	}

	public static void main(String[] args) throws Exception {
		PkNN_kdist2_Adapt rs = new PkNN_kdist2_Adapt();
		rs.run(args);
	}
}