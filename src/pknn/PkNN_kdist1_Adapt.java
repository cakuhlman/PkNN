/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */
package pknn;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

import metricspace.IMetric;
import metricspace.IMetricSpace;
import metricspace.MetricFactory.L2Metric;
import metricspace.MetricObject;
import metricspace.MetricSpaceFactory.VectorSpace;
import metricspace.MetricSpaceUtility;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import util.Config;
import util.Pivot;
import util.PivotInfo;
import util.PivotTree;
import util.Record;

import com.infomatiq.jsi.Point;
import com.infomatiq.jsi.Rectangle;

/**
 * Process partition and calculate k-distance for core area (1) Use DataSplit
 * Mapper to assign points to pivots (2) in reduce function, compute k-distance
 * for each object in core area.
 * 
 * @author Yizhou modified Luwei
 * @author Cailtin Kuhlman 
 * 
 */
public class PkNN_kdist1_Adapt {

	private static int dim;

	public static class PkKdist1Mapper extends
			Mapper<LongWritable, Text, LongWritable, Text> {
		/**
		 * VectorSpace and L1 are set as the default metric space and metric.
		 */
		private IMetricSpace metricSpace = null;
		private IMetric metric = null;
		private int dim;
		private HashSet<Integer> dropped;
		private PivotTree pivots;
		// track number of points in each partition
		private int[] numOfObjects;
		int K;
		/** intermediate key */
		private LongWritable pivotKey = new LongWritable();
		private Text pointValue = new Text();

		/** number of object pairs to be computed */
		static enum Counters {
			sum
		}

		/**
		 * get MetricSpace and metric from configuration
		 * 
		 * @param conf
		 * @throws IOException
		 */
		private void readMetricAndMetricSpace(Configuration conf)
				throws IOException {

			metricSpace = new VectorSpace();
			metric = new L2Metric();
			metricSpace.setMetric(metric);
		}

		/**
		 * read pivots from input
		 * 
		 * @param conf
		 */
		private void readPivotFile(Configuration conf, Context context)
				throws IOException {
			URI[] pivotFiles = new URI[0];

			pivotFiles = context.getCacheFiles();
			if (pivotFiles == null || pivotFiles.length < 1)
				throw new IOException("No pivots are provided!");

			// check if any pivots dropped during data splitting
			for (URI path : pivotFiles) {
				String filename = path.toString();
				if (filename.endsWith(Config.strDropExpression))
					dropped = getDropped(filename, metricSpace, dim, conf);
			}
			// only load pivots if needed
			if (dropped.size() != 0) {
				for (URI path : pivotFiles) {
					String filename = path.toString();
					if (filename.endsWith(Config.strPivotExpression))
						pivots = buildPivTree(filename, metricSpace, dim, conf);
				}
			}
		}

		private HashSet<Integer> getDropped(String filename,
				IMetricSpace metricSpace2, int dim2, Configuration conf)
				throws IOException {
			FileSystem fs = FileSystem.get(conf);
			BufferedReader fis = null;
			FSDataInputStream in = fs.open(new Path(filename));

			try {
				fis = new BufferedReader(new InputStreamReader(in));
				String line;
				HashSet<Integer> dropped = new HashSet<Integer>();
				while ((line = fis.readLine()) != null) {
					dropped.add(Integer.parseInt(line));
				}
				return dropped;
			} catch (IOException ioe) {
				System.err
						.println("Caught exception while parsing the cached file '"
								+ filename + "'");
				return null;
			} finally {
				if (in != null) {
					fis.close();
					in.close();
				}
			}

		}

		/**
		 * read pivots from a single file
		 * 
		 * @param pivotFile
		 * @return
		 * @throws IOException
		 */
		public PivotTree buildPivTree(String pivotFile,
				IMetricSpace metricSpace, int dim,
				org.apache.hadoop.conf.Configuration conf) throws IOException {

			pivots = new PivotTree();
			pivots.init(null);

			FileSystem fs = FileSystem.get(conf);
			BufferedReader fis = null;
			FSDataInputStream in = fs.open(new Path(pivotFile));

			try {
				fis = new BufferedReader(new InputStreamReader(in));
				String line;
				Pivot p;
				while ((line = fis.readLine()) != null) {
					p = Pivot.readPivot(line, dim);
					pivots.add(new Rectangle(p.getValue()[0], p.getValue()[1],
							p.getValue()[0], p.getValue()[1]), p.getId());
				}
				return pivots;
			} catch (IOException ioe) {
				System.err
						.println("Caught exception while parsing the cached file '"
								+ pivotFile + "'");
				return null;
			} finally {
				if (in != null) {
					fis.close();
					in.close();
				}
			}
		}

		/**
		 * Called once at the beginning of the task. In setup, we (1) init
		 * metric and metric space (2) get number of pivots (3) init matrix
		 */
		protected void setup(Context context) throws IOException,
				InterruptedException {
			Configuration conf = context.getConfiguration();
			/** get K */
			K = Integer.valueOf(conf.get(Config.strK, "3"));
			/** get dim */
			dim = conf.getInt(Config.strDimExpression, 2);
			/** read metric */
			readMetricAndMetricSpace(conf);
			/** read pivot set */
			readPivotFile(conf, context);
		}

		/**
		 * We generate
		 * 
		 * (1) key: pivot id , pivot bounding distance
		 * 
		 * (2) value: point id , point vals, distance to pivot
		 * 
		 * @key: offset of the source file
		 * @value: format: point_id, point_vals
		 * 
		 */
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {

			String[] s = value.toString().split("\t");
			int partId = Integer.parseInt(s[0]);
			if (dropped.size() != 0) {

				if (dropped.contains(partId)) {

					// handle points that were mapped to cells with < k points
					Record r = metricSpace.readRecord(s[1], dim);
					PivotInfo p = pivots.nearestPivot(new Point(
							r.getValue()[0], r.getValue()[1]));
					// map point to this closest pivot
					pivotKey.set(p.getId());
					// trim old dist to piv
					StringBuffer sb = new StringBuffer(s[1].substring(0,
							s[1].lastIndexOf(",")));
					sb.append(",");
					sb.append(p.getDist()); // dist to new pivot
					// value = id, point_vals, dist to pivot
					pointValue.set(sb.toString());
					context.write(pivotKey, pointValue);

				} else {
					pivotKey.set(partId);
					pointValue.set(s[1]);
					context.write(pivotKey, pointValue);

				}
			} else {
				pivotKey.set(partId);
				pointValue.set(s[1]);
				context.write(pivotKey, pointValue);
			}
		}
	}

	public static class PkKdist1Reducer extends
			Reducer<LongWritable, Text, LongWritable, Text> {
		MultipleOutputs<LongWritable, Text> mos;
		private IMetricSpace metricSpace = null;
		private IMetric metric = null;
		int k;
		LongWritable outputKey = new LongWritable();
		Text outputValue = new Text();
		Text newPivotValue = new Text();
		private int partitionId;
		float boundingDistance;
		private int maxPivot;
		private HashMap<Integer, Pivot> pivots;

		/**
		 * get MetricSpace and metric from configuration
		 * 
		 * @param conf
		 * @throws IOException
		 */
		private void readMetricAndMetricSpace(Configuration conf)
				throws IOException {

			metricSpace = new VectorSpace();
			try {
				metric = MetricSpaceUtility.getMetric(conf
						.get(Config.strMetric));
			} catch (Exception e) {
				e.printStackTrace();
			}
			metricSpace.setMetric(metric);
		}

		/**
		 * read pivots from input
		 * 
		 * @param conf
		 */
		private void readPivotFile(Configuration conf, Context context)
				throws IOException {
			URI[] pivotFiles = new URI[0];

			pivotFiles = context.getCacheFiles();
			if (pivotFiles == null || pivotFiles.length < 1)
				throw new IOException("No pivots are provided!");

			for (URI path : pivotFiles) {
				String filename = path.toString();
				if (filename.endsWith(Config.strPivotExpression))
					pivots = readPivots(filename, metricSpace, dim, conf);
			}
		}

		/**
		 * read pivots from a single file, only parse piv, boundingdist for
		 * early output
		 * 
		 * @param pivotFile
		 * @return
		 * @throws IOException
		 */
		public HashMap<Integer, Pivot> readPivots(String pivotFile,
				IMetricSpace metricSpace, int dim,
				org.apache.hadoop.conf.Configuration conf) throws IOException {
			HashMap<Integer, Pivot> pivots = new HashMap<Integer, Pivot>();
			FileSystem fs = FileSystem.get(conf);
			BufferedReader fis = null;
			FSDataInputStream in = fs.open(new Path(pivotFile));
			try {
				fis = new BufferedReader(new InputStreamReader(in));
				String line;
				while ((line = fis.readLine()) != null) {
					Pivot p = Pivot.readPivotAdapt(line, dim);
					pivots.put(p.getId(), p);
				}
				return pivots;
			} catch (IOException ioe) {
				System.err
						.println("Caught exception while parsing the cached file '"
								+ pivotFile + "'");
				return null;
			} finally {
				if (in != null) {
					fis.close();
					in.close();
				}
			}
		}

		protected void setup(Context context) throws IOException,
				InterruptedException {

			mos = new MultipleOutputs<LongWritable, Text>(context);
			Configuration conf = context.getConfiguration();
			dim = conf.getInt(Config.strDimExpression, 2);
			readMetricAndMetricSpace(conf);
			k = Integer.valueOf(conf.get(Config.strK, "3"));
			readPivotFile(conf, context);
			maxPivot = conf.getInt(Config.strNumOfPivots, 0) + 1;
		}

		/**
		 * strInput format: point_id, point_vals, dist to pivot
		 */
		private MetricObject parseObject(long partition_id, String strInput) {

			String[] inputSplits = strInput.split(",");
			// get point id
			long pid = Long.parseLong(inputSplits[0]);

			// parse coordinates of original point
			float[] values = new float[dim];

			for (int i = 1; i < dim + 1; i++) {
				values[i - 1] = Float.parseFloat(inputSplits[i]);
			}
			// get dist to pivot last entry
			float dist = Float.valueOf(inputSplits[inputSplits.length - 1]);
			return new MetricObject(pid, dist, partition_id, values);
		}

		/**
		 * Find knn among core points in partition, evaluate which points
		 * complete after one pass, and handle global outliers.
		 * 
		 * We generate
		 * 
		 * (1) key: pivot id , pivot bounding distance
		 * 
		 * (2) value: output format: point_id, point_vals, dist to pivot,
		 * k-distance, (KNN's nid and dist), completion tag
		 * 
		 * @param key
		 *            : pivot id , pivot bounding distance
		 * 
		 * @param values
		 *            : point id , point vals, distance to pivot
		 */
		public void reduce(LongWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {

			partitionId = (int) key.get();

			// Sort all points by distance to pivot
			ArrayList<MetricObject> sortedData = new ArrayList<MetricObject>();
			for (Text value : values) {
				MetricObject mo = parseObject(partitionId, value.toString());
				sortedData.add(mo);
			}
			Collections.sort(sortedData, new Comparator<MetricObject>() {
				public int compare(MetricObject map1, MetricObject map2) {
					return Float.compare(map2.distToPivot, map1.distToPivot);
				}
			});

			if (sortedData.size() < k + 1) {
				throw new IOException("IN KDIST 1 ONLY " + sortedData.size()
						+ " POINTS ASSIGNED TO PARTITION " + partitionId);
			}
			// find knn for each core point, keep track of max kdist, supDist
			PivotBasedKNNSearch search = new PivotBasedKNNSearch(metric, k);

			float maxKdist = 0; // max kdist of a point
			float maxSupDist = 0; // max of dist from a point to the pivot + its
									// kdist

			for (int i = 0; i < sortedData.size(); i++) {
				MetricObject thisPoint = sortedData.get(i);
				String line = search.findKNNFirstRoundPivot(thisPoint, i,
						sortedData);

				float kdist = search.getLastKDist();

				// check if point is complete
				if (Float.compare(kdist + thisPoint.distToPivot,
						pivots.get(partitionId).getDist()) < 0) {
					// mark as complete
					line += ",c";
					// don't count kdist, suppdist
				}
				// Check against adaptive bound for cell
				// Mark point and assign as new pivot if global outlier
				else if (Float.compare(kdist, pivots.get(partitionId)
						.getThresh()) > 0) {
					line = line + "," + partitionId + ",o";
					// new pivots need unique ids
					long newPivotId = thisPoint.getId() + maxPivot;
					outputKey.set(newPivotId);
					newPivotValue.set(newPivotId + thisPoint.toString() + ","
							+ kdist +"," + partitionId);
					mos.write("globalOutliers", NullWritable.get(),
							newPivotValue);
					// don't count kdist, supDist
				}

				else {
					// mark as incomplete
					line += ",i";

					// update stats for bounds
					if (Float.compare(maxKdist, kdist) < 0) {
						maxKdist = kdist;
					}
					float supDist = thisPoint.distToPivot + (kdist);

					if (Float.compare(maxSupDist, supDist) < 0) {
						maxSupDist = supDist;
					}
				}
				outputValue.set(line);
				outputKey.set(thisPoint.partition_id);
				context.write(outputKey, outputValue);
			}

			outputPivot(partitionId, maxKdist, maxSupDist, sortedData.size(),
					context);
		}

		private void outputPivot(long partitionId, float maxKdist,
				float maxSupDist, int count, Context context)
				throws IOException, InterruptedException {
			outputKey.set(partitionId);
			outputValue.set(maxKdist + "," + maxSupDist + "," + count);
			mos.write("pivotKdist", outputKey, outputValue);
		}

		@Override
		public void cleanup(Context context) throws IOException,
				InterruptedException {
			mos.close();
		}
	}

	public void run(String[] args) throws Exception {

		/********************** SETUP *********************************/
		Configuration conf = new Configuration();
		conf.addResource(new Path(
				"/usr/local/Cellar/hadoop/etc/hadoop/core-site.xml"));
		conf.addResource(new Path(
				"/usr/local/Cellar/hadoop/etc/hadoop/hdfs-site.xml"));

		new GenericOptionsParser(conf, args).getRemainingArgs();
		Job job = Job.getInstance(conf, "Random Pivot Selection");
		FileSystem fs = FileSystem.get(conf);
		Path input = new Path(conf.get(Config.dataset));
		Path output = new Path(conf.get(Config.pivotOutput));

		int dim = conf.getInt(Config.strDimExpression, 2);
		int k = conf.getInt(Config.strK, 3);

		long begin;
		long end;
		long seconds;
		Counters counters;
		CounterGroup group;

		/******************* RUN KDIST 1 *******************************/

		job = Job.getInstance(conf, "Calculate k-distance round 1");
		job.setJarByClass(PkNN_kdist1_Adapt.class);
		job.setMapperClass(PkKdist1Mapper.class);
		// job.setOutputKeyClass(Text.class);
		// job.setOutputValueClass(Text.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setReducerClass(PkKdist1Reducer.class);

		MultipleOutputs.addNamedOutput(job, "pivotKdist",
				TextOutputFormat.class, LongWritable.class, Text.class);
		MultipleOutputs.addNamedOutput(job, "globalOutliers",
				TextOutputFormat.class, NullWritable.class, Text.class);
		
//		String strFSName = conf.get("fs.default.name");
		String strFSName = "/home/caitlin/Documents/PkNN_public/";
		
		input = new Path(strFSName + conf.get(Config.strLofInput));
		output = new Path(strFSName + conf.get(Config.strKdistance1Output));
		job.setNumReduceTasks(conf.getInt(Config.strNumOfReducers, 1));
		
		job.addCacheFile(new URI(strFSName + conf.get(Config.strPivotInput)));
		job.addCacheFile(new URI(strFSName + conf.get(Config.strPivotDrop)));
		FileInputFormat.addInputPath(job, input);
		fs = FileSystem.get(conf);
		fs.delete(output, true);
		FileOutputFormat.setOutputPath(job, output);

		System.err.println("Running kdist 1");

		/** print job parameter */
		System.err.println("input path: " + input);
		System.err.println("output path: " + output);
		System.err.println("pivot file: " + conf.get(Config.strPivotInput));
		System.err.println("dataspace: " + conf.get(Config.strMetricSpace));
		System.err.println("metric: " + conf.get(Config.strMetric));
		System.err.println("value of K: " + k);
		System.err.println("# of groups: "
				+ conf.get(Config.strNumOfReducers));
		System.err.println("# of dim: "
				+ conf.getInt(Config.strDimExpression, 2));

		begin = System.currentTimeMillis();
		job.waitForCompletion(true);

		end = System.currentTimeMillis();
		seconds = (end - begin) / 1000;

		counters = job.getCounters();
		group = counters.getGroup("org.apache.hadoop.mapreduce.TaskCounter");
		// System.out.println("Method, datasize, num_pivots, time");
		System.out.print("kdist_1,");
		System.out.print(group.findCounter("MAP_INPUT_RECORDS").getValue()
				+ ","); // size of dataset
		System.out.print(group.findCounter("REDUCE_INPUT_GROUPS").getValue()
				+ ","); // number of pivots
		System.out.println(seconds);

		/*********************** GET OUTPUT *****************************/

		fs = FileSystem.get(conf);
		FileStatus[] stats = fs.listStatus(output);
		InputStream is = null;

		OutputStream kdist = fs.create(new Path(strFSName
				+ conf.get(Config.strPivotKDistInput)));
		OutputStream global = fs.create(new Path(strFSName
				+ conf.get(Config.outliers)));

		try {
			for (int i = 0; i < stats.length; ++i) {
				String fileName = output.toString() + "/"
						+ stats[i].getPath().getName();

				if (fileName.contains("pivotKdist")) {
					is = new BufferedInputStream(fs.open(new Path(fileName)));
					IOUtils.copyBytes(is, kdist, conf, false);
					fs.delete(stats[i].getPath(), true);
					is.close();

				} else if (fileName.contains("globalOutliers")) {
					is = new BufferedInputStream(fs.open(new Path(fileName)));
					IOUtils.copyBytes(is, global, conf, false);
					fs.delete(stats[i].getPath(), true);
					is.close();
				}
			}
		} finally {
			global.close();
			kdist.close();

		}
	}

	public static void main(String[] args) throws Exception {
		PkNN_kdist1_Adapt rs = new PkNN_kdist1_Adapt();
		rs.run(args);
	}

}
