/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */

package pknn;

import java.io.IOException;
import java.util.ArrayList;

import metricspace.IMetric;
import metricspace.MetricObject;

import org.apache.hadoop.mapreduce.Reducer.Context;

import util.PriorityQueue;
import util.Config;

/**
 * Simple pivot-based indexing method used to speed local knn search in PkNN.
 * @author caitlin kuhlman
 *
 */
public class PivotBasedKNNSearch {

	private IMetric metric = null;
	private int k;
	private float lastKdist;

	public PivotBasedKNNSearch(IMetric metric, int k) {
		this.metric = metric;
		this.k = k;
	}

	/**
	 * Use pivot-based index to find knn of point, formats output value for
	 * single round knn search.
	 * 
	 * @param thisPoint
	 * @param currentIndex
	 *            index of thisPoint in the sortedData.
	 * @param sortedData
	 *            list of other points in partition ordered by distance to
	 *            pivot.
	 * @return output value string.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public String findKNNSingleRound(MetricObject thisPoint, int currentIndex,
			ArrayList<MetricObject> sortedData) throws IOException,
			InterruptedException {

		PriorityQueue pq = findNeighborsPivot(thisPoint, currentIndex,
				sortedData);
		String line = "";

		line += thisPoint.partition_id + Config.sepStrForRecord
				+ thisPoint.getId() + Config.sepStrForRecord + pq.getPriority()
				+ Config.sepStrForRecord + thisPoint.whoseSupport
				+ Config.sepStrForRecord;

		if (pq.size() > 0) {
			line += pq.getValue() + Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		while (pq.size() > 0) {
			line += Config.sepStrForRecord + pq.getValue()
					+ Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		return line;
	}

	/**
	 * Use pivot-based index to find knn of point, formats output value for
	 * single round knn search.
	 * 
	 * @param thisPoint
	 * @param currentIndex
	 *            index of thisPoint in the sortedData.
	 * @param sortedData
	 *            list of other points in partition ordered by distance to
	 *            pivot.
	 * @param context
	 * @return output value string.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public String findKNNSingleRoundNL(MetricObject thisPoint,
			int currentIndex, ArrayList<MetricObject> sortedData,
			Context context) throws IOException, InterruptedException {

		PriorityQueue pq = findNeighborsNL(thisPoint, sortedData, context);
		String line = "";

		line += thisPoint.partition_id + Config.sepStrForRecord
				+ thisPoint.getId() + Config.sepStrForRecord + pq.getPriority()
				+ Config.sepStrForRecord + thisPoint.whoseSupport
				+ Config.sepStrForRecord;

		if (pq.size() > 0) {
			line += pq.getValue() + Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		while (pq.size() > 0) {
			line += Config.sepStrForRecord + pq.getValue()
					+ Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		return line;
	}

	/**
	 * Use pivot-based index to find knn, formats output value for first round
	 * knn search and updates kdist value.
	 * 
	 * @param thisPoint
	 *            to find the knn of.
	 * @param currentIndex
	 *            index of thisPoint in the sortedData.
	 * @param sortedData
	 *            list of other points in partition ordered by distance to
	 *            pivot.=
	 * @return output value string.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public String findKNNFirstRoundPivot(MetricObject thisPoint,
			int currentIndex, ArrayList<MetricObject> sortedData)
			throws IOException, InterruptedException {

		PriorityQueue pq = findNeighborsPivot(thisPoint, currentIndex,
				sortedData);
		String line = "";

		// output format: point_id, point_vals, dist to pivot, k-distance,
		// (KNN's nid and dist), completion tag

		lastKdist = pq.getPriority();

		String vals = "";
		for (int v = 0; v < thisPoint.record.getValue().length; v++) {
			vals += thisPoint.record.getValue()[v] + ",";
		}
		line += thisPoint.getId() + Config.sepStrForRecord + vals
				+ thisPoint.distToPivot + Config.sepStrForRecord
				+ pq.getPriority() + Config.sepStrForRecord;
		if (pq.size() < k) {
			throw new IOException("IN KDIST 1 ONLY " + pq.size()
					+ " NEIGHBORS FOUND FOR POINT: " + thisPoint.getId()
					+ "IN PARTITION: " + thisPoint.partition_id);
		}
		if (pq.size() > 0) {
			line += pq.getValue() + Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		while (pq.size() > 0) {
			line += Config.sepStrForRecord + pq.getValue()
					+ Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		return line;
	}

	/**
	 * Use pivot-based index to find knn, formats output value for first round
	 * knn search and updates kdist value.
	 * 
	 * @param thisPoint
	 *            to find the knn of.
	 * @param currentIndex
	 *            index of thisPoint in the sortedData.
	 * @param sortedData
	 *            list of other points in partition ordered by distance to
	 *            pivot.=
	 * @return output value string.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public String findKNNFirstRoundNL(MetricObject thisPoint, int currentIndex,
			ArrayList<MetricObject> sortedData) throws IOException,
			InterruptedException {

		PriorityQueue pq = findNeighborsNL(thisPoint, sortedData, null);
		String line = "";

		// output format: point_id, point_vals, dist to pivot, k-distance,
		// (KNN's nid and dist), completion tag

		lastKdist = pq.getPriority();

		String vals = "";
		for (int v = 0; v < thisPoint.record.getValue().length; v++) {
			vals += thisPoint.record.getValue()[v] + ",";
		}
		line += thisPoint.getId() + Config.sepStrForRecord + vals
				+ thisPoint.distToPivot + Config.sepStrForRecord
				+ pq.getPriority() + Config.sepStrForRecord;
		if (pq.size() < k) {
			throw new IOException("IN KDIST 1 ONLY " + pq.size()
					+ " NEIGHBORS FOUND FOR POINT: " + thisPoint.getId()
					+ "IN PARTITION: " + thisPoint.partition_id);
		}
		if (pq.size() > 0) {
			line += pq.getValue() + Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		while (pq.size() > 0) {
			line += Config.sepStrForRecord + pq.getValue()
					+ Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		return line;
	}

	/**
	 * Use pivot-based index to find knn in second round, formats output value
	 * for first round knn search and updates kdist value.
	 * 
	 * @param thisPoint
	 *            to find the knn of.
	 * @param currentIndex
	 *            index of thisPoint in the sortedData.
	 * @param sortedData
	 *            list of other points in partition ordered by distance to
	 *            pivot.=
	 * @return output value string.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public String findKNNSecondRoundPivot(MetricObject thisPoint,
			int currentIndex, ArrayList<MetricObject> sortedData)
			throws IOException, InterruptedException {

		PriorityQueue pq = findNeighborsPivotSecondRound(thisPoint,
				currentIndex, sortedData);

		// FINAL OUTPUT FORMAT: point_id <tab> myPartiton_id,
		// thisParition_id, kdist, whoseSupport, neighbors
		String line = "";
		// format: partition id, distToPivot, p_vals, k-distance,
		// (KNN's nid and dist), completion tag

//		LOD INFO FOR LOF
//		line += thisPoint.partition_id + SQConfig.sepStrForRecord
//				+ thisPoint.partition_id + SQConfig.sepStrForRecord
//				+ pq.getPriority() + SQConfig.sepStrForRecord
//				+ thisPoint.whoseSupport + SQConfig.sepStrForRecord;

		if (pq.size() > 0) {
			line += pq.getValue() + Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		while (pq.size() > 0) {
			line += Config.sepStrForRecord + pq.getValue()
					+ Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		return line;
	}

	/**
	 * Use pivot-based index to find knn in second round, formats output value
	 * for first round knn search and updates kdist value.
	 * 
	 * @param thisPoint
	 *            to find the knn of.
	 * @param currentIndex
	 *            index of thisPoint in the sortedData.
	 * @param sortedData
	 *            list of other points in partition ordered by distance to
	 *            pivot.=
	 * @return output value string.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public String findKNNSecondRoundNL(MetricObject thisPoint,
			int currentIndex, ArrayList<MetricObject> sortedData, Context context)
			throws IOException, InterruptedException {

		PriorityQueue pq = findNeighborsNLSecondRound(thisPoint,
				currentIndex, sortedData, context);

		// FINAL OUTPUT FORMAT: point_id <tab> myPartiton_id,
		// thisParition_id, kdist, whoseSupport, neighbors
		String line = "";
		// format: partition id, distToPivot, p_vals, k-distance,
		// (KNN's nid and dist), completion tag

		line += thisPoint.partition_id + Config.sepStrForRecord
				+ thisPoint.partition_id + Config.sepStrForRecord
				+ pq.getPriority() + Config.sepStrForRecord
				+ thisPoint.whoseSupport + Config.sepStrForRecord;

		if (pq.size() > 0) {
			line += pq.getValue() + Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		while (pq.size() > 0) {
			line += Config.sepStrForRecord + pq.getValue()
					+ Config.sepStrForIDDist + pq.getPriority();
			pq.pop();
		}
		return line;
	}

	/**
	 * Find neighbors of this point from sortedData list.
	 * 
	 * @param thisPoint
	 * @param currentIndex
	 * @param sortedData
	 * @param pq
	 */
	private PriorityQueue findNeighborsPivot(MetricObject thisPoint,
			int currentIndex, ArrayList<MetricObject> sortedData) {

		PriorityQueue pq = new PriorityQueue(
				PriorityQueue.SORT_ORDER_DESCENDING);

		float dist;
		float theta = Float.POSITIVE_INFINITY;
		boolean kNNfound = false;
		int inc_current = currentIndex + 1;
		int dec_current = currentIndex - 1;
		float i = 0, j = 0; // i---increase j---decrease

		while ((!kNNfound)
				&& ((inc_current < sortedData.size()) || (dec_current >= 0))) {

			if ((inc_current > sortedData.size() - 1) && (dec_current < 0))
				break;
			if (inc_current > sortedData.size() - 1)
				i = Float.MAX_VALUE;
			if (dec_current < 0)
				j = Float.MAX_VALUE;
			if (Float.compare(i, j) <= 0) {
				MetricObject thatPoint = sortedData.get(inc_current);
				dist = metric.distFromFloat(thisPoint.record.getValue(), thatPoint.record.getValue());
				theta = updateQueue(dist, pq, theta, thatPoint.getId());
				inc_current += 1;
				i = Math.abs(thisPoint.distToPivot - thatPoint.distToPivot);
			} else {
				MetricObject thatPoint = sortedData.get(dec_current);
				dist = metric.distFromFloat(thisPoint.record.getValue(), thatPoint.record.getValue());
				theta = updateQueue(dist, pq, theta, thatPoint.getId());
				dec_current -= 1;
				j = Math.abs(thisPoint.distToPivot - thatPoint.distToPivot);
			}
			if (Float.compare(i, pq.getPriority()) > 0
					&& Float.compare(j, pq.getPriority()) > 0
					&& (pq.size() == k))
				kNNfound = true;
		}
		return pq;
	}

	/**
	 * Find neighbors of this point from sortedData list.
	 * 
	 * @param thisPoint
	 * @param currentIndex
	 * @param sortedData
	 * @param pq
	 */
	private PriorityQueue findNeighborsNL(MetricObject thisPoint,
			ArrayList<MetricObject> sortedData, Context context) {

		PriorityQueue pq = new PriorityQueue(
				PriorityQueue.SORT_ORDER_DESCENDING);

		float dist;
		float theta = Float.POSITIVE_INFINITY;
		int count = 0;

		for (MetricObject thatPoint : sortedData) {
			dist = metric.distFromFloat(thisPoint.record.getValue(), thatPoint.record.getValue());
			theta = updateQueue(dist, pq, theta, thatPoint.getId());
			count++;
			// report progress to avoid timeout
			if (count % 100 == 0) {
				context.progress();
			}
		}
		return pq;
	}

	/**
	 * Find neighbors of this point from sortedData list.
	 * 
	 * @param thisPoint
	 * @param currentIndex
	 * @param sortedData
	 * @param pq
	 * @throws IOException
	 */
	private PriorityQueue findNeighborsPivotSecondRound(MetricObject thisPoint,
			int currentIndex, ArrayList<MetricObject> sortedData)
			throws IOException {

		PriorityQueue pq = new PriorityQueue(
				PriorityQueue.SORT_ORDER_DESCENDING);

		// include neighbors found in previous round
		String[] split;
		for (String n : thisPoint.knnInfo) {
			split = n.split("\\|");

			pq.insert(Long.parseLong(split[0]), Float.parseFloat(split[1]));
		}

		float dist;
		float theta = pq.getPriority();
		boolean kNNfound = false;
		int inc_current = currentIndex + 1;
		int dec_current = currentIndex - 1;
		float i = 0, j = 0; // i---increase j---decrease

		MetricObject thatPoint;
		while ((!kNNfound)
				&& ((inc_current < sortedData.size()) || (dec_current >= 0))) {

			if ((inc_current > sortedData.size() - 1) && (dec_current < 0))
				break;
			if (inc_current > sortedData.size() - 1)
				i = Float.MAX_VALUE;
			if (dec_current < 0)
				j = Float.MAX_VALUE;

			if (Float.compare(i, j) <= 0) {
				thatPoint = sortedData.get(inc_current);
				// only check support points
				if (thatPoint.type == 'S') {
					dist = metric.distFromFloat(thisPoint.record.getValue(),
							thatPoint.record.getValue());
					theta = updateQueue(dist, pq, theta, thatPoint.getId());
					i = Math.abs(thisPoint.distToPivot - thatPoint.distToPivot);
				}
				inc_current += 1;
			} else {
				thatPoint = sortedData.get(dec_current);
				if (thatPoint.type == 'S') {
					dist = metric.distFromFloat(thisPoint.record.getValue(),
							thatPoint.record.getValue());
					theta = updateQueue(dist, pq, theta, thatPoint.getId());
					j = Math.abs(thisPoint.distToPivot - thatPoint.distToPivot);
				}
				dec_current -= 1;
			}
			if (Float.compare(i, pq.getPriority()) > 0
					&& Float.compare(j, pq.getPriority()) > 0
					&& (pq.size() == k))
				kNNfound = true;
		}
		return pq;
	}

	/**
	 * Find neighbors of this point from sortedData list.
	 * 
	 * @param thisPoint
	 * @param currentIndex
	 * @param sortedData
	 * @param pq
	 * @throws IOException
	 */
	private PriorityQueue findNeighborsNLSecondRound(MetricObject thisPoint,
			int currentIndex, ArrayList<MetricObject> sortedData,
			Context context) throws IOException {

		PriorityQueue pq = new PriorityQueue(
				PriorityQueue.SORT_ORDER_DESCENDING);

		// include neighbors found in previous round
		String[] split;
		for (String n : thisPoint.knnInfo) {
			split = n.split("\\|");

			pq.insert(Integer.parseInt(split[0]), Float.parseFloat(split[1]));
		}

		float dist;
		float theta = pq.getPriority();

		int count = 0;

		for (MetricObject thatPoint : sortedData) {
			if (thatPoint.type == 'S') {
				dist = metric.distFromFloat(thisPoint.record.getValue(), thatPoint.record.getValue());
				theta = updateQueue(dist, pq, theta, thatPoint.getId());
			}
			count++;
			// report progress to avoid timeout
			if (count % 100 == 0) {
				context.progress();
			}
		}
		return pq;
	}

	/**
	 * Retrieve the kdist value for the last point evaluated.
	 * 
	 * @return null if no knn search has been conducted previously.
	 */
	public float getLastKDist() {
		return lastKdist;
	}

	/**
	 * Check is that point is closer that points in queue, break ties using
	 * point ids.
	 * 
	 * @param dist
	 * @param pq
	 * @param theta
	 * @param thatPoint
	 * @return
	 */
	public float updateQueue(float dist, PriorityQueue pq, float theta, long pid) {
		if (pq.size() < k) {
			pq.insert(pid, dist);
		}
		// break tie using element with larger id
		else if (Float.compare(dist, theta) == 0) {
			long old_id = pq.pop();
			if (old_id < pid) {
				pq.insert(pid, dist);
			} else {
				pq.insert(old_id, dist);
			}
		}
		// update with closer object
		else if (Float.compare(dist, theta) < 0) {
			pq.pop();
			pq.insert(pid, dist);
		}
		theta = pq.getPriority();
		return theta;
	}
}
