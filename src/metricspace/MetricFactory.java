/* metricspace package implementation provided by authors of 
 * Lu, Wei, et al. "Efficient processing of k nearest neighbor joins using mapreduce." 
 * Proceedings of the VLDB Endowment 5.10 (2012): 1016-1027.*/
package metricspace;

/**
 * 
 * @author luwei
 * 
 */
import java.io.IOException;

import util.Record;

public class MetricFactory {

	public static class L1Metric extends IMetric {
		
		/**
		 * L1 distance: the sum of |v1[i]-v2[i]| for any i 
		 */
		public float dist(Record r1, Record r2) throws IOException {
			numOfDistComp = numOfDistComp + 1;
			return Record.compL1Dist(r1,r2);
//			return Record.compL1DistFromFloat((float[])o1, (float[])o2);
		}
		
		/**
		 * L2 distance: the sqrt of sum (v1[i]-v2[i])*(v1[i]-v2[i]) for any i 
		 */
		public float distFromFloat(float[] o1, float[] o2) {
			numOfDistComp = numOfDistComp + 1;
			return Record.compL1DistFromFloat(o1, o2);
		}	

		public long getNumOfDistComp() {
			return numOfDistComp;
		}
	}

	public static class L2Metric extends IMetric {

		
		/**
		 * L2 distance: the sqrt of sum (v1[i]-v2[i])*(v1[i]-v2[i]) for any i 
		 */
		public float dist(Record r1, Record r2) throws IOException {
			numOfDistComp = numOfDistComp + 1;
			return Record.compL2Dist(r1, r2);
		}

		/**
		 * L2 distance: the sqrt of sum (v1[i]-v2[i])*(v1[i]-v2[i]) for any i 
		 */
		public float distFromFloat(float[] o1, float[] o2) {
			numOfDistComp = numOfDistComp + 1;
			return Record.compL2DistFromFloat(o1, o2);
		}	
			
		public long getNumOfDistComp() {
			return numOfDistComp;
		}
	}

}
