/* metricspace package implementation provided by authors of 
 * Lu, Wei, et al. "Efficient processing of k nearest neighbor joins using mapreduce." 
 * Proceedings of the VLDB Endowment 5.10 (2012): 1016-1027.*/
package metricspace;

/**
 * 
 * @author luwei
 * 
 */
import java.io.IOException;

import util.Record;


public class MetricSpaceFactory {
	public static class VectorSpace implements IMetricSpace {
		private static final String regex = ",";
		private IMetric metric;

		public VectorSpace() {

		}

		public VectorSpace(IMetric metric) {
			this.metric = metric;
		}

		public void setMetric(IMetric metric) {
			this.metric = metric;
		}

		public long getID(Record r) {
			return r.getRId();
		}
		
		@Override
		/**
		 * parse a vector object from a given line
		 */
		public Record readRecord(String line, int dim) {
			if (line == null || line == "") {
				return null;
			}

			/**
			 * format of the line: rid,dim1,dim2,...,dimN,desc
			 */
			String[] strVector = line.split(regex);
			Long rid = Long.valueOf(strVector[0]);
			float[] dVector = new float[dim];
			for (int i = 0; i < dim; i++) {
				dVector[i] = Float.valueOf(strVector[i + 1]);
			}

			return new Record(rid, dVector);
		}

		@Override
		public float compDist(Record r1, Record r2) throws IOException {
			return metric.dist(r1, r2);
		}

		@Override
		public String outputObject(Record r) {

			return r.toString();
		}
	}
}
