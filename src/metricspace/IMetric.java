/* metricspace package implementation provided by authors of 
 * Lu, Wei, et al. "Efficient processing of k nearest neighbor joins using mapreduce." 
 * Proceedings of the VLDB Endowment 5.10 (2012): 1016-1027.*/
package metricspace;

/**
 * 
 * @author luwei
 * 
 */
import java.io.IOException;

import util.Record;

public abstract class IMetric {
	public long numOfDistComp = 0;

	public abstract float dist(Record r1, Record r2) throws IOException;

	public abstract long getNumOfDistComp();

	public abstract float distFromFloat(float[] values, float[] values2);
}
