/* metricspace package implementation provided by authors of 
 * Lu, Wei, et al. "Efficient processing of k nearest neighbor joins using mapreduce." 
 * Proceedings of the VLDB Endowment 5.10 (2012): 1016-1027.*/
package metricspace;

/**
 * 
 * @author luwei
 * 
 */
import java.io.IOException;

import util.Record;

public interface IMetricSpace {
	public Record readRecord(String line, int dim);
	public void setMetric(IMetric metric);
	public float compDist(Record r1, Record r2) throws IOException ;
	public String outputObject(Record r);
	public long getID(Record r);
}