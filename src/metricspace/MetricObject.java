/* metricspace package implementation provided by authors of 
 * Lu, Wei, et al. "Efficient processing of k nearest neighbor joins using mapreduce." 
 * Proceedings of the VLDB Endowment 5.10 (2012): 1016-1027.*/
package metricspace;

/**
 * 
 * @author luwei
 * 
 */
import java.util.HashMap;
import java.util.Map;

import util.Record;

@SuppressWarnings("rawtypes")
public class MetricObject implements Comparable {
	public long partition_id;
	public char type;
	public float distToPivot;
	public Record record;
	public String whoseSupport="";
	public String KNN = "";
	public String[] knnInfo;
	public Map<Integer,Float> knnInDetail = new HashMap<Integer,Float>();
	public float kdist = 0;
	public float lrd = 0;
	public boolean canCalculateLof = false;
	
	public MetricObject() {
	}

	public MetricObject(Record record) {
		this.record = record;
	}
	public MetricObject(long pid, float dist, long partition_id, float[] values) {
		this.distToPivot = dist;
		this.partition_id = partition_id;
		this.record = new Record(pid, values);
	}
	public MetricObject(long pid, float dist, char type, long partition_id, String whoseSupport, Record record) {
		this.distToPivot = dist;
		this.type = type;
		this.partition_id = partition_id;
		this.record = record;
		this.whoseSupport = whoseSupport;
	}
	public MetricObject(float dist, char type, long partition_id, String whoseSupport, Record record) {
		this.distToPivot = dist;
		this.type = type;
		this.partition_id = partition_id;
		this.record = record;
		this.whoseSupport = whoseSupport;
	}
	public MetricObject(long pid, float dist, char type, long partition_id, String whoseSupport, Record record, float kdistance, String KNN){
		this(pid,dist,type,partition_id,whoseSupport,record);
		this.KNN = KNN;
		this.kdist = kdistance;
	}
	public MetricObject(long pid, long part_id,float distToPivot, float kdist, String whoseSupport, String[] knnInfo, Record record, char tag) {
		
		this.partition_id = part_id;
		this.distToPivot = distToPivot;
		this.kdist = kdist;
		this.knnInfo = knnInfo;
		this.record = record;
		this.type = tag;
		this.whoseSupport = whoseSupport;
		
	}

	public MetricObject(long pid, Record record, float distToPivot) {
		this.distToPivot = distToPivot;
		this.record = record;
	}

	public MetricObject(long pid, Record record, float kdist, String[] knnInfo) {
		this.distToPivot = kdist;
		this.record = record;
		this.knnInfo = knnInfo;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(); 

		for (float v : this.record.getValue()) {
			sb.append("," + v);
		}
		return sb.toString();
	}
	
	/**
	 * sort by the descending order
	 */
	@Override
	public int compareTo(Object o) {
		MetricObject other = (MetricObject) o;
		if (other.distToPivot > this.distToPivot)
			return 1;
		else if (other.distToPivot < this.distToPivot)
			return -1;
		else
			return 0;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
	}
	
	public long getId(){
		return this.record.getRId();
	}
	
}
