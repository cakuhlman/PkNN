/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */
package preprocess;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * Adapted from 
 * @author caitlin
 *
 */
public class PivotTree {

	public static void main(String[] args) {

		BufferedReader br = null;
		HashMap<Integer, Point> map = new HashMap<Integer, Point>();
		ArrayList<Point> points = new ArrayList<Point>();

		try {
			// initialize files
			br = new BufferedReader(new FileReader("pivots"));
			String line = null;

			while ((line = br.readLine()) != null) {
				String[] s = line.split(",");
				int id = Integer.parseInt(s[0]);
				float x = Float.parseFloat(s[1]);
				float y = Float.parseFloat(s[2]);
				float dist = Float.parseFloat(s[3]);
				int nid = Integer.parseInt(s[4]);
				Point p = new Point(id, x, y, nid, dist);
				map.put(id, p);
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		try {
			br = new BufferedReader(new FileReader("data/small_test.csv"));
			String line = null;
			while ((line = br.readLine()) != null) {
				line = null;

				while ((line = br.readLine()) != null) {
					String[] s = line.split(",");
					float x = Float.parseFloat(s[1]);
					float y = Float.parseFloat(s[2]);

					points.add(new Point(x, y));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		Collections.shuffle(points);
		System.out.println("Start");
		int moves = 0;
		int allmoves = 0;
		for (Point p : points) {

 			moves = 0;
			int counter = 0;
			float minDist = Float.MAX_VALUE;
			int closest = -1;
			List<Integer> keyList = new ArrayList<Integer>(map.keySet());
			int num_pivots = map.size();
			while (counter < num_pivots) 
			{
				if (counter != p.id) 
				{
					Point thatPoint = map.get(keyList.get(counter));
					float d = p.dist(thatPoint);
					if (d < minDist) 
					{
						minDist = d;
						closest = thatPoint.id;
					} 
					else 
					{
						if (thatPoint.neighbor > counter && d - thatPoint.dist > minDist) 
						{
							//remove pivot neighbor if too far away (can make recursive to prune more points)
							keyList.remove(thatPoint.neighbor);
							//keep track how many points we remove
							moves++;
							allmoves++;
							num_pivots--;
						}
					}
				}
				//move to next pivot
				counter++;
			}
			//store closest pivot info in point
			p.neighbor = closest;
			p.dist = minDist;
			System.out.println("Point: " + p.id + "\tMoves: "+moves);
		}
		
		for (Point p : points) {
			System.out.println("Point: " + p.id + "\tneighbor: "+p.neighbor);
		}
		System.out.println("Average moves saved " + allmoves/(float) points.size());
	}
}

class Point {
	int id;
	float x;
	float y;
	int neighbor;
	float dist;

	Point(int id, float x, float y, int neighbor, float dist) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.neighbor = neighbor;
		this.dist = dist;
	}

	Point(float x, float y) {
		this.x = x;
		this.y = y;
	}

	float dist(Point p) {
		float dist = ((this.x - p.x) * (this.x - p.x))
				+ ((this.y - p.y) * (this.y - p.y));
		return (float) Math.sqrt(dist);
	}

}
