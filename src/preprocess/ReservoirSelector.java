/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */

package preprocess;

import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import util.Config;

/**
 * 
 * @author caitlin kuhlman
 *
 */
public class ReservoirSelector {
	static int dim;

	/**
	 * 
	 * Mappers select small percentage of points as pivots.
	 *
	 */
	public static class ReservoirMapper extends
			Mapper<LongWritable, Text, FloatWritable, Text> {

		/** Probability a point is selected as a pivot **/
		private int numPivots;
		private static Random rand;
		private static PriorityQueue<Point> pq;

		/** Output key **/
		private static FloatWritable outputKey = new FloatWritable(0);

		/** Output value **/
		private static Text outputValue = new Text();

		protected void setup(Context context) throws IOException,
				InterruptedException {
			Configuration conf = context.getConfiguration();
			numPivots = conf.getInt(Config.strNumOfPivots, 0);
			rand = new Random();
			pq = new PriorityQueue<Point>(numPivots);
		}

		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			
			//keep up to the desired number of pivots
			if(pq.size() < numPivots){
				pq.add(new Point(value.toString(), rand.nextFloat()));
			}
			//keep the points with the lowest random values
			else {
				float r = rand.nextFloat();
				//head of queue has highest value
				float f = pq.peek().p;
				if (f > r){
					pq.poll();
					pq.add(new Point(value.toString(), r));
				}
			}
		}
		
		/**
		 * Output list of pivots.
		 */
		protected void cleanup(Context context) throws IOException,
				InterruptedException {

			for (Point p : pq) {
				outputKey.set(p.p);
				outputValue.set(p.s);
				context.write(outputKey, outputValue);
			}
		}
	}

	static class Point implements Comparable<Point>{
		String s;
		float p;
		
		Point(String s, float p){
			this.s = s;
			this.p = p;
			
		}

		@Override
		public int compareTo(Point o) {
			return Float.compare(o.p, this.p);
		}
	}
	/**
	 * 
	 * Single reducer selects top n pivots output by reduces.
	 * Performs pairwise comparison of pivots and assigns
	 * distance to closest pivot as interior bound.
	 *
	 */
	public static class ReservoirReducer extends
			Reducer<FloatWritable, Text, NullWritable, Text> {

		static int dim;
		static int numPivots;
		static int numSelected = 0;
		static Text outputValue = new Text();
		

		protected void setup(Context context) throws IOException,
				InterruptedException {
			Configuration conf = context.getConfiguration();
			dim = conf.getInt(Config.strDimExpression, 0);
			numPivots = conf.getInt(Config.strNumOfPivots, 0);
		}

		/**
		 * Select top n pivots from randomly shuffled selection;
		 * 
		 * @param key
		 *            : 0
		 * @param values
		 *            : list of randomly sampled pivots
		 */
		public void reduce(FloatWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			
			//output pivots with k lowest keys (hadoop automatically sorts before sending to reducer)
			for (Text value : values){
				if(numSelected < numPivots){
					
					//build pivot string
					String[] split = value.toString().split(",");
					StringBuffer sb = new StringBuffer();
					sb.append(numSelected);
					for (int i=0 ; i<dim ; i++){
						sb.append(",");
						sb.append(split[i+1]);
					}
					outputValue.set(sb.toString());
					context.write(NullWritable.get(), outputValue);
					numSelected++;
				}
				else{
					break;
				}
			}
		}
	}

	public void run(String[] args) throws Exception {

		Configuration conf = new Configuration();
		conf.addResource(new Path(
				"/usr/local/Cellar/hadoop/etc/hadoop/core-site.xml"));
		conf.addResource(new Path(
				"/usr/local/Cellar/hadoop/etc/hadoop/hdfs-site.xml"));
		new GenericOptionsParser(conf, args).getRemainingArgs();
		// /** set job parameter */
		Job job = Job.getInstance(conf, "Random Pivot Selector");

		job.setJarByClass(ReservoirSelector.class);
		job.setMapperClass(ReservoirMapper.class);
		job.setReducerClass(ReservoirReducer.class);

		job.setMapOutputKeyClass(FloatWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setNumReduceTasks(1);

		String input = conf.get(Config.dataset);
		String output = conf.get(Config.pivotOutput);
		FileInputFormat.addInputPath(job, new Path(input));
		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(conf.get(Config.pivotOutput)), true);
		FileOutputFormat.setOutputPath(job, new Path(output));

		// /** print job parameter */
		System.err.println("input path: " + input);
		System.err.println("output path: " + output);
		System.err.println("dataspace: " + conf.get(Config.strMetricSpace));
		System.err.println("metric: " + conf.get(Config.strMetric));
		System.err.println("# of groups: "
				+ conf.get(Config.strNumOfReducers));
		System.err.println("# of dim: "
				+ conf.getInt(Config.strDimExpression, 2));

		long begin = System.currentTimeMillis();
		job.waitForCompletion(true);
		long end = System.currentTimeMillis();
		long seconds = (end - begin) / 1000;

		Counters counters = job.getCounters();
		CounterGroup group = counters
				.getGroup("org.apache.hadoop.mapreduce.TaskCounter");
		System.out.println("Method, datasize, num_pivots, time");
		System.out.print("piv_select,");
		System.out.print(group.findCounter("MAP_INPUT_RECORDS").getValue()
				+ ",");
		System.out.print(group.findCounter("REDUCE_OUTPUT_RECORDS").getValue()
				+ ","); // number of pivots
		System.out.println(seconds);

//		String strFSName = conf.get("fs.default.name");
		String strFSName = "/home/caitlin/Documents/PkNN_public/";
		// get pivot file
		String pivotFile = strFSName + conf.get(Config.strPivotInput);
		fs.rename(new Path(strFSName + output.toString() + "/part-r-00000"), new Path(pivotFile));

	}

	public static void main(String[] args) throws Exception {
		ReservoirSelector rs = new ReservoirSelector();
		rs.run(args);
	}
}