/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */
package preprocess;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import metricspace.IMetric;
import metricspace.IMetricSpace;
import metricspace.MetricFactory.L2Metric;
import metricspace.MetricSpaceFactory.VectorSpace;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import util.Pivot;
import util.Config;

/**
 * Update threshold values for a pivot list.
 * 
 * @author Caitlin Kuhlman
 * 
 */
public class AvgUpdateThresh {

	
//	USAGE: tune,  
	public static void main(String[] args) throws Exception {
		
 
		/********************** SETUP *********************************/
		Configuration conf = new Configuration();

		//get tuning param for adaptive bound
		Float tune = 1f;
		/*get max number of points for each machine*/
		int m = Integer.parseInt(args[0]);
		//parse rest of args
		args = Arrays.copyOfRange(args, 1, args.length);
		new GenericOptionsParser(conf, args).getRemainingArgs();
		FileSystem fs = FileSystem.get(conf);
		/** get dim */
		int dim = conf.getInt(Config.strDimExpression, 2);
		
		String strFSName = conf.get("fs.default.name");
		System.err.println("Running Update Pivot");
		System.err.println("Tune" + tune);

		/*********************** GET OUTPUT *****************************/

		fs = FileSystem.get(conf);
		
		ArrayList<Pivot> pivots = new ArrayList<Pivot>();
		int[] sizes;
		float[] dists;
		float[] avgs;
		InputStream is = null;
		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			// read pivots (after datasplit and thresh assignment from previous test)
//	
			is = new BufferedInputStream(fs.open(new Path(strFSName + conf.get(Config.strPivotInput))));
			br = new BufferedReader(new InputStreamReader(is));
			Pivot p = null;
			for (String line = br.readLine(); line != null; line = br
					.readLine()) {
				String[] data = line.split(",");
				
//				format:id, vals, ibound, size, maxDist, avgDist, thresh
				int pivId = Integer.parseInt(data[0]);
				float[] vals = new float[dim];
				int j;
				for (j = 0; j < dim; j++) {
					vals[j] = Float.parseFloat(data[j + 1]);
				}
				float ibound = Float.parseFloat(data[j + 1]);
				p = new Pivot(pivId, vals);
				p.setDist(ibound);
				pivots.add(p);
				}
			is.close();

//			read stats
			sizes = new int[pivots.size()];
			dists = new float[pivots.size()];
			avgs = new float[pivots.size()];
	
			is = new BufferedInputStream(fs.open(new Path(strFSName + conf.get(Config.strPivotInput))));
			br = new BufferedReader(new InputStreamReader(is));
			for (String line = br.readLine(); line != null; line = br
					.readLine()) {
				String[] data = line.split(",");
				
//				format:id, vals, ibound, size, maxDist, avgDist, thresh
				int pivId = Integer.parseInt(data[0]);
				sizes[pivId]= Integer.parseInt(data[dim+2]);
				dists[pivId]= Float.parseFloat(data[dim+3]);
				avgs[pivId]= Float.parseFloat(data[dim+4]);
				}
			is.close();
			br.close();
		} 
		finally {
			is.close();
			br.close();
			if(bw != null){
				bw.close();
			}
		}
	
		/* *********************** ADAPTIVE BOUND ************** */
		boolean valid = true;
		float[] thresh = new float[pivots.size()];
		// compute threshold for each cell
		for (int i = 0; i < pivots.size(); i++) {

//			check if enough core points
			if(Float.compare(sizes[i], m) > 0){
				valid=false;
			}
//			 compute density of cell
			double d_avg = (double) sizes[i]
					/ Math.pow((double) avgs[i], (double) dim);
			thresh[i] = tune * (float) (Math.pow(m / d_avg, 1.0 / dim) - avgs[i]);
		}

		/* *********************** ADAPTIVE BOUND ***************** */

		// replace pivot file
		fs.delete(new Path(strFSName + conf.get(Config.strPivotInput)), true);
		try {
			// write out pivot, bound, size file
			OutputStream piv = fs.create(new Path(strFSName
					+ conf.get(Config.strPivotInput)));
			bw = new BufferedWriter(new OutputStreamWriter(piv));

			for (int i = 0; i < pivots.size(); i++) {
				Pivot p = pivots.get(i);
				StringBuffer sb = new StringBuffer(p.toString());
				sb.append(",");
				sb.append(p.getDist());
				sb.append(",");
				sb.append(sizes[i]);
				sb.append(",");
				sb.append(dists[i]);
				sb.append(",");
				sb.append(avgs[i]);
				sb.append(",");
				sb.append(thresh[i]);
				bw.write(sb.toString());
				bw.write("\n");
			}
			bw.close();
		} finally {
			bw.close();
		}
		
		//get max pivot id to assign new pivots to global outliers
		int maxPivot = conf.getInt(Config.strNumOfPivots, 0)  + 1;
		conf.setInt("maxPivot", maxPivot);
		
		if(valid = false){
			System.out.println("Too many core points!! m= "+m+" tune= "+tune);
			throw new Exception("Too many core points!! m= "+m+" tune= "+tune);
		}
		
		System.out.println("done");
	}
}
