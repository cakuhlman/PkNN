/*
 * Copyright (C) 2017 Worcester Polytechnic Institute All Rights Reserved.
 */
package preprocess;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import metricspace.IMetric;
import metricspace.IMetricSpace;
import metricspace.MetricFactory.L2Metric;
import metricspace.MetricSpaceFactory.VectorSpace;
import metricspace.MetricSpaceUtility;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import util.Config;
import util.Pivot;
import util.Record;

/**
 * Perform pivot-based data partitioning.
 * 
 * @author Caitlin Kuhlman
 * 
 */
public class AvgPartitioner {

	/**
	 * Read each point and map it to the closest pivot. output format: Key:
	 * pivotid Value: point id, point_vals, dist to pivot
	 * @author caitlin
	 *
	 */
	public static class SplitMapperAdapt extends
			Mapper<Object, Text, Text, Text> {

		MultipleOutputs<Text, Text> mos;// "cellSize"
		private IMetricSpace metricSpace = null;
		private IMetric metric = null;
		private int dim;

		private Vector<Pivot> pivots;
		// track number of points in each partition
		private int[] numOfObjects;
		// track distance to farthest point in each partition
		private float[] farthestDist;
		private float[] avgDist;
		int k;

		/** intermediate key */
		private Text pivotKey = new Text();
		private Text pointValue = new Text();

		/** number of object pairs to be computed */
		static enum Counters {
			sum
		}

		/**
		 * get MetricSpace and metric from configuration
		 * 
		 * @param conf
		 * @throws IOException
		 */
		private void readMetricAndMetricSpace(Configuration conf)
				throws IOException {

			metricSpace = new VectorSpace();
			metric = new L2Metric();
			metricSpace.setMetric(metric);
		}

		/**
		 * read pivots from input
		 * 
		 * @param conf
		 */
		private void readPivotFile(Configuration conf, Context context)
				throws IOException {
			URI[] pivotFiles = new URI[0];

			pivotFiles = context.getCacheFiles();

			if (pivotFiles == null || pivotFiles.length < 1)
				throw new IOException("No pivots are provided!");

			for (URI path : pivotFiles) {
				String filename = path.toString();
				if (filename.endsWith(Config.strPivotExpression))
					pivots = readPivots(filename, (VectorSpace) metricSpace,
							dim, conf);
			}
		}

		/**
		 * read pivots from a single file
		 * 
		 * @param pivotFile
		 * @return
		 * @throws IOException
		 */
		public Vector<Pivot> readPivots(String pivotFile,
				VectorSpace metricSpace, int dim,
				org.apache.hadoop.conf.Configuration conf) throws IOException {

			Vector<Pivot> pivots = new Vector<Pivot>();
			FileSystem fs = FileSystem.get(conf);
			BufferedReader fis = null;
			FSDataInputStream in = fs.open(new Path(pivotFile));
			try {
				fis = new BufferedReader(new InputStreamReader(in));
				String line;
				while ((line = fis.readLine()) != null) {
					pivots.add(Pivot.readPivot(line, dim));
				}
				return pivots;
			} catch (IOException ioe) {
				System.err
						.println("Caught exception while parsing the cached file '"
								+ pivotFile + "'");
				return null;
			} finally {
				if (in != null) {
					fis.close();
					in.close();
				}
			}
		}

		/**
		 * Initialize fields once per mapper.
		 */
		protected void setup(Context context) throws IOException,
				InterruptedException {

			Configuration conf = context.getConfiguration();
			mos = new MultipleOutputs<Text, Text>(context);

			k = Integer.valueOf(conf.get(Config.strK, "3"));
			dim = conf.getInt(Config.strDimExpression, 2);

			readMetricAndMetricSpace(conf);

			readPivotFile(conf, context);

			// holds the number of core points in each partition
			numOfObjects = new int[pivots.size()];
			// holds the dist to farthest point in each partition
			farthestDist = new float[pivots.size()];
			avgDist = new float[pivots.size()];
			// initialize avg to -1
			Arrays.fill(avgDist, -1);
		}

		/**
		 *
		 * @key: offset of the source file
		 * @value: format: point_id, point_vals
		 * 
		 */
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {

			String line = value.toString();

			/** parse the object */
			Record r = metricSpace.readRecord(line, dim);

			int i, closestPivotId = 0;
			float closestDist = Float.MAX_VALUE;
			float tmpDist = 0f;
			int test_dist;

			/** find the closest pivot */
			for (i = 0; i < pivots.size(); i++) {
				tmpDist = metric.dist(pivots.get(i), r);
				test_dist = Float.compare(tmpDist, closestDist);

				// break ties by assigning to partition with fewer points
				if (test_dist == 0
						&& numOfObjects[closestPivotId] > numOfObjects[i]) {
					closestDist = tmpDist;
					closestPivotId = i;
				} else if (test_dist < 0) {
					closestDist = tmpDist;
					closestPivotId = i;
				}
			}

			// check if farthest point in partition
			if (Float.compare(closestDist, farthestDist[closestPivotId]) > 0) {
				farthestDist[closestPivotId] = closestDist;
			}

			// track average distance of points to pivot
			if (avgDist[closestPivotId] == -1) {
				// first point in partition
				avgDist[closestPivotId] = closestDist;
			} else {
				avgDist[closestPivotId] = ((avgDist[closestPivotId] * numOfObjects[closestPivotId]) + closestDist)
						/ (numOfObjects[closestPivotId] + 1);
			}
			// update number of points in each partition
			numOfObjects[closestPivotId]++;

			// map point to this closest pivot
			// key = pivot,boundingDist
			Pivot closestPivot = pivots.get(closestPivotId);
			pivotKey.set(String.valueOf(closestPivot.getId()));

			// value = id, point_vals, dist to pivot
			pointValue.set(line + "," + closestDist);
			context.write(pivotKey, pointValue);
		}

		@Override
		public void cleanup(Context context) throws IOException,
				InterruptedException {

			StringBuffer sb;
			// output stats for partition: pivot id \t size \t farthest dist
			for (int i = 0; i < numOfObjects.length; i++) {
				pivotKey.set(String.valueOf(i));
				sb = new StringBuffer(String.valueOf(numOfObjects[i]));
				sb.append("\t");
				sb.append(farthestDist[i]);
				sb.append("\t");
				sb.append(avgDist[i]);
				pointValue.set(sb.toString());
				mos.write("cellSize", pivotKey, pointValue);
			}
			mos.close();
		}
	}

	public void run(String[] args) throws Exception {
		/********************** SETUP *********************************/
		Configuration conf = new Configuration();
		conf.addResource(new Path(
				"/usr/local/Cellar/hadoop/etc/hadoop/core-site.xml"));
		conf.addResource(new Path(
				"/usr/local/Cellar/hadoop/etc/hadoop/hdfs-site.xml"));

		// get tuning param for adaptive bound
		int m = Integer.parseInt(args[0]);
		// parse rest of args
		args = Arrays.copyOfRange(args, 1, args.length);
		new GenericOptionsParser(conf, args).getRemainingArgs();
		Job job = Job.getInstance(conf, "Random Pivot Selection");
		FileSystem fs = FileSystem.get(conf);
		Path input = new Path(conf.get(Config.dataset));
		Path output = new Path(conf.get(Config.pivotOutput));

		IMetric metric = MetricSpaceUtility.getMetric(conf
				.get(Config.strMetric));
		int dim = conf.getInt(Config.strDimExpression, 2);
		int k = conf.getInt(Config.strK, 3);

		/******************* RUN DATA SPLIT *******************************/

		job = Job.getInstance(conf, "Data Split");
		job.setJarByClass(AvgPartitioner.class);
		job.setMapperClass(SplitMapperAdapt.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setReducerClass(Reducer.class);

		MultipleOutputs.addNamedOutput(job, "cellSize", TextOutputFormat.class,
				Text.class, Text.class);

//		String strFSName = conf.get("fs.default.name");
		String strFSName = "/home/caitlin/Documents/PkNN_public/";
		
		input = new Path(strFSName + conf.get(Config.dataset));
		output = new Path(strFSName + conf.get(Config.strLofInput));
		job.setNumReduceTasks(conf.getInt(Config.strNumOfReducers, 1));
		job.addCacheFile(new URI(strFSName + conf.get(Config.strPivotInput)));
		FileInputFormat.addInputPath(job, input);
		fs.delete(output, true);
		FileOutputFormat.setOutputPath(job, output);

		System.err.println("Running datasplit");

		/** print job parameter */
		System.err.println("input path: " + input);
		System.err.println("output path: " + output);
		System.err.println("pivot file: " + conf.get(Config.strPivotInput));
		System.err.println("# of dim: "
				+ conf.getInt(Config.strDimExpression, 2));
		System.err.println("value of K: " + k);

		System.err.println("Max partition size: " + m);

		long begin = System.currentTimeMillis();
		job.waitForCompletion(true);

		long end = System.currentTimeMillis();
		long seconds = (end - begin) / 1000;

		Counters counters = job.getCounters();
		CounterGroup group = counters
				.getGroup("org.apache.hadoop.mapreduce.TaskCounter");

		System.out.print("datasplit,");
		System.out.print(m + ","); // m
		System.out.print(group.findCounter("MAP_INPUT_RECORDS").getValue()
				+ ","); // size of dataset
		System.out.print(group.findCounter("REDUCE_INPUT_GROUPS").getValue()
				+ ","); // number of pivots

		System.out.println(seconds);

		/*********************** GET OUTPUT *****************************/

		FileStatus[] stats = fs.listStatus(output);

		int[] sizes;
		float[] dists;
		float[] avgs;
		ArrayList<Pivot> pivots = new ArrayList<Pivot>();
		InputStream is = null;
		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			// read original pivots
			is = new BufferedInputStream(fs.open(new Path(strFSName
					+ conf.get(Config.strPivotInput))));
			br = new BufferedReader(new InputStreamReader(is));
			for (String line = br.readLine(); line != null; line = br
					.readLine()) {
				String[] data = line.split(",");
				int pivId = Integer.parseInt(data[0]);
				float[] vals = new float[dim];
				for (int j = 0; j < dim; j++) {
					vals[j] = Float.parseFloat(data[j + 1]);
				}
				pivots.add(new Pivot(pivId, vals));
			}
			is.close();
			br.close();

			// indices of pivot list used in stats arrays
			sizes = new int[pivots.size()];
			dists = new float[pivots.size()];
			avgs = new float[pivots.size()];
			Arrays.fill(avgs, -1f);

			// read cell counts for each pivot
			OutputStream size = fs.create(new Path(strFSName
					+ conf.get(Config.strSizes)));
			bw = new BufferedWriter(new OutputStreamWriter(size));

			for (int i = 0; i < stats.length; ++i) {
				String fileName = output.toString() + "/"
						+ stats[i].getPath().getName();

				if (fileName.contains("cellSize")) {

					// read in cell stat info
					is = new BufferedInputStream(fs.open(new Path(fileName)));
					br = new BufferedReader(new InputStreamReader(is));

					// parse data
					for (String line = br.readLine(); line != null; line = br
							.readLine()) {
						String[] data = line.split("\t");
						int pivId = Integer.parseInt(data[0].trim());
						int count = Integer.parseInt(data[data.length - 3]);
						float dist = Float.parseFloat(data[data.length - 2]);
						float avg = Float.parseFloat(data[data.length - 1]);

						// update avg dists from all mappers
						if (avgs[pivId] == -1) {
							avgs[pivId] = avg;
						} else {
							avgs[pivId] = ((avgs[pivId] * sizes[pivId]) + (avg * count))
									/ (sizes[pivId] + count);
						}

						// aggregate point count from all mappers
						sizes[pivId] += count;

						// get max of dists from all mappers
						dists[pivId] = Math.max(dist, dists[pivId]);

					}
					// cleanup cell size file
					fs.delete(stats[i].getPath(), true);
				}
			}
			for (int i = 0; i < sizes.length; i++) {
				bw.write(i + "," + sizes[i] + "\n");
			}
		} finally {
			is.close();
			br.close();
			if (bw != null) {
				bw.close();
			}
		}

		// eliminate pivots with less than k points
		ArrayList<Integer> dropped = new ArrayList<Integer>();

		for (int i = 0; i < pivots.size(); i++) {

			if (sizes[i] < k + 1) {
				dropped.add(pivots.get(sizes[i]).getId());
				pivots.remove(i);
				i--;
			}
		}

		// compute bounding distance for rest of pivots
		for (Pivot p : pivots) {
			float minDist = Float.MAX_VALUE;

			for (Pivot q : pivots) {
				if (p.getId() != q.getId()) {
					float dist = p.dist(q);
					if (((Float) dist).compareTo(minDist) < 0) {
						minDist = dist;
					}
				}
			}
			p.setDist(minDist / 2);
		}

		/* *********************** ADAPTIVE BOUND ************** */

		boolean valid = true;
		float[] thresh = new float[pivots.size()];
		// compute threshold for each cell
		for (int i = 0; i < pivots.size(); i++) {

			// check if enough core points
			if (Float.compare(sizes[i], m) > 0) {
				valid = false;
			}
			// compute density of cell using avg dist as radius
			double d = (double) sizes[i]
					/ Math.pow((double) avgs[i], (double) dim);

			thresh[i] = (float) (Math.pow(m / d, 1.0 / dim) - avgs[i]);
		}

		/* *********************** ADAPTIVE BOUND ***************** */

		// replace pivot file
		fs.delete(new Path(strFSName + conf.get(Config.strPivotInput)), true);
		try {
			// write out pivot, bound, size file
			OutputStream piv = fs.create(new Path(strFSName
					+ conf.get(Config.strPivotInput)));
			bw = new BufferedWriter(new OutputStreamWriter(piv));
			// output:id, vals, ibound, size, maxDist, avgDist, thresh
			for (int i = 0; i < pivots.size(); i++) {
				Pivot p = pivots.get(i);
				StringBuffer sb = new StringBuffer(p.toString());
				sb.append(",");
				sb.append(p.getDist());
				sb.append(",");
				sb.append(sizes[i]);
				sb.append(",");
				sb.append(dists[i]);
				sb.append(",");
				sb.append(avgs[i]);
				sb.append(",");
				sb.append(thresh[i]);
				bw.write(sb.toString());
				bw.write("\n");
			}
			bw.close();
		} finally {
			bw.close();
		}

		// replace pivot file
		fs.delete(new Path(strFSName + conf.get(Config.strPivotDrop)), true);
		try {
			// write out pivot, bound, size file
			OutputStream piv = fs.create(new Path(strFSName
					+ conf.get(Config.strPivotDrop)));
			bw = new BufferedWriter(new OutputStreamWriter(piv));
			// output:id, vals, ibound, size, maxDist, avgDist, thresh
			for (int i = 0; i < dropped.size(); i++) {
				bw.write(dropped.get(i).toString());
				bw.write("\n");
			}
			bw.close();
		} finally {
			bw.close();
		}

		// get max pivot id to assign new pivots to global outliers
		int maxPivot = conf.getInt(Config.strNumOfPivots, 0) + 1;
		conf.setInt("maxPivot", maxPivot);

		if (valid == false) {
			System.out.println("Too many core points!! m= " + m);
		}
	}

	public static void main(String[] args) throws Exception {
		AvgPartitioner rs = new AvgPartitioner();
		rs.run(args);
	}

}
